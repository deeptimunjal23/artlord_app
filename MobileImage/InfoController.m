//
//  InfoController.m
//  MobileImage
//
//  Created by Anil on 18/10/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import "InfoController.h"
#import "AppDelegate.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <Photos/Photos.h>
#import "MobileImage-Swift.h"

@interface InfoController ()

@property(nonatomic, strong) LivePhoto *livePhoto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end

@implementation InfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized: {
            }
                break;
                
            case PHAuthorizationStatusRestricted:
            case PHAuthorizationStatusDenied:
                break;
                
            default:
                break;
        }
    }];
    
    
 }



-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.indicator.hidden = true;
    [self.indicator stopAnimating];
    
    myPhotoArray = [[NSMutableArray alloc]init];
    
    _heightLayoutView.constant = 667 ;
    [self.view layoutIfNeeded];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showAlert) name:@"ShowAlert" object:nil];
    
    self.livePhoto = [self.storyboard instantiateViewControllerWithIdentifier:@"LivePhoto"];
    [self addChildViewController:self.livePhoto];
    
    [self.view addSubview:self.livePhoto.view];
    self.livePhoto.view.hidden = true;
    [self.livePhoto didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.livePhoto.view];
    _imgGradient.hidden = NO ;
    
    
    _imgPhotos.layer.cornerRadius = 15.0f;
    [_imgPhotos clipsToBounds];
    _imgPhotos.layer.masksToBounds = YES;
    [self.view layoutIfNeeded];
    
    
    _txtDescription.text = _descriptionStr ;
    _txtThemeName.text = _nameStr ;
    _txtArtist.text = _collectionStr ;
    _scllView.delegate = self;
    
    [_btnDownloadSecond setHidden:YES];
    [self.view bringSubviewToFront:_bottomView];
    
    
    [self.view layoutIfNeeded];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self updateTime];
    self.imgBig.image = _imageName;
    
    
    if ([_VideoNameStr containsString:@"mp4"]) {
        
        videoStr  = _VideoNameStr ;
        
        
        NSLog(@"Video Str %@",videoStr);
        
        NSArray* foo = [videoStr componentsSeparatedByString: @"."];
        NSString* namee = [foo objectAtIndex: 0];
        
        NSString*type = [foo objectAtIndex:1];
        NSString *filepath =[[NSBundle mainBundle] pathForResource: namee ofType:type inDirectory:nil];
        NSURL *fileURL    =   [NSURL fileURLWithPath:filepath];
        self.av_player = [AVPlayer playerWithURL:fileURL];
        
        self.av_layer = [AVPlayerLayer layer];
        
        [self.av_layer setPlayer:self.av_player];
        
        self.av_layer.frame = CGRectMake(0, 0,_imgInfo.frame.size.width, _imgInfo.frame.size.height);
        [self.av_layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        UIView *subViews = [[UIView alloc]init];
        
        subViews.frame =  CGRectMake(0, 0,_imgInfo.frame.size.width,_imgInfo.frame.size.height );
        [subViews.layer addSublayer:self.av_layer];
        
        [self.scrollView addSubview:subViews];
        
        [_av_player play];
        
        
        
        
        
    }else{
        
        self.imgInfo.image = _imageName;
        
    }
    
    
    _txtThemeNamee.text =_nameStr;
    _txtThemeCollection.text =_collectionStr ;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(updateTime)
                                   userInfo:nil
                                    repeats:YES];
    

    
    [self.view bringSubviewToFront:self.back_btn];
    
    [self.view bringSubviewToFront: _Save_btn];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    _Save_btn.alpha =0.0 ;
  
    
     [self.view bringSubviewToFront:_ViewBorder];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)showAlert {
    self.indicator.hidden = true;
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Animated Image Downloaded Successfully." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}
- (void)updateTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    if (showBlinkTimer) {
        [formatter setDateFormat:@"h mm"];
        showBlinkTimer = false;
        
    }
    else {
        [formatter setDateFormat:@"h:mm"];
        showBlinkTimer = true;
        
    }
    NSString *timeString = [formatter stringFromDate:[NSDate date]];
    
    self.Time_lbe.text = timeString;
    
    
    formatter.dateFormat=@"dd";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    
    formatter.dateFormat=@"MMMM";
    NSString *monthString = [[formatter stringFromDate:[NSDate date]] capitalizedString];
    
    formatter.dateFormat=@"EEEE";
    NSString *dayString = [[formatter stringFromDate:[NSDate date]] capitalizedString];
    
    _Weekday_lbe.text=[NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,dateString];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back_btn:(id)sender {
   
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    

}
- (IBAction)homeClicked:(id)sender {
  
     [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(_imgBig.image) forKey:@"img"];
    
    [self dismissViewControllerAnimated:false completion:^{
      AppDelegate *app =(AppDelegate*)[UIApplication sharedApplication].delegate;
      [app GoToRootViewController];
  }];
}



- (IBAction)themesClciked:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}

- (BOOL)image:(UIImage*)image1 isEqualTo:(UIImage*)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}


- (IBAction)downloadCClicked:(id)sender {
       ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    
      if ([_VideoNameStr containsString:@"mp4"]) {
          
          
          NSArray* foo = [_VideoNameStr componentsSeparatedByString: @"."];
          NSString* namee = [foo objectAtIndex: 0];
          
          NSString*type = [foo objectAtIndex:1];

          NSURL *fileURL  =   [[NSBundle mainBundle]URLForResource:namee withExtension:type];//[NSURL fileURLWithPath:filepath];
          self.indicator.hidden = false;
          [self.indicator startAnimating];
          [[NSUserDefaults standardUserDefaults]setValue:namee forKey:@"url"];
          [[NSUserDefaults standardUserDefaults]synchronize];
          
          [self.livePhoto loadVideoWithVideoURL:fileURL];
          //self.livePhoto = [self.storyboard instantiateViewControllerWithIdentifier:@"LivePhoto"];

         // [self presentViewController:self.livePhoto animated:YES completion:nil];
          
      }
          else{
  
    [library saveImage: self.imgBig.image toAlbum:@"Themes Album" withCompletionBlock:^(NSError *error) {
        if (error!=nil)
        {
            NSLog(@"Noooo error: %@", [error description]);
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Image Downloaded Successfully." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            });

        }
        
         }];
          
      }
    
    }
    

- (IBAction)moreClicked:(id)sender{
    
}
    
- (void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName
    {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        [library enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
                //If album found
                [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                    //add asset to album
                    [group addAsset:asset];
                    
                } failureBlock:nil];
            }
            else {
                //if album not found create an album
                [library addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group)     {
                    [self addAssetURL:assetURL toAlbum:albumName];
                    
                } failureBlock:nil];
            }
        } failureBlock: nil];
    }
//+ (PHLivePhotoRequestID)requestLivePhotoWithResourceFileURLs:(NSArray<NSURL *> *)fileURLs placeholderImage:(UIImage *__nullable)image targetSize:(CGSize)targetSize contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(PHLivePhoto *__nullable livePhoto, NSDictionary *info))resultHandler{
//    
//}
   @end
