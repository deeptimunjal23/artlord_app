//
//  MyThemesController.h
//  MobileImage
//
//  Created by Anil on 18/10/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>


@interface MyThemesController : UIViewController {
    id allkeys ;
    NSMutableArray *arrayCollection;
    NSMutableArray *arrayDescription;
    NSMutableArray *arrayid;
    NSMutableArray *arrayName;
    NSMutableArray *keys;
    
    NSString *videoStr ;
    UIView *subView ;
}

@property (strong, nonatomic) AVPlayer * av_player;
@property (strong, nonatomic) AVPlayerLayer * av_layer;
@property (strong, nonatomic) NSMutableArray *arrayPlayer;

@property (strong, nonatomic) IBOutlet UICollectionView *FreeTheme_CollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *SaveTheme_CollectionView;
- (IBAction)Back:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnsave_Leading;
- (IBAction)Save_btn:(id)sender;
- (IBAction)Free_btn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lable_leading;
@property (weak, nonatomic) IBOutlet UILabel *line_Label;
- (IBAction)homeBtn:(id)sender;

//@property(weak,nonatomic)UIImage *imgTheme;
@property (nonatomic , retain) UIImage *imgTheme;
@property (nonatomic , retain) NSMutableArray *arrayImages;
@property (nonatomic , retain) NSMutableArray *arrayCollection;
@property (nonatomic , retain) NSMutableArray *arrayDescription;
@property (nonatomic , retain) NSMutableArray *arrayName;
@property (nonatomic , retain) NSMutableArray *arrayid;





@end
