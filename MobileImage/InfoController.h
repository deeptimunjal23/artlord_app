//
//  InfoController.h
//  MobileImage
//
//  Created by Anil on 18/10/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface InfoController : UIViewController<UIScrollViewDelegate>{
    BOOL showBlinkTimer;
     NSMutableArray *myPhotoArray;
    
    NSString *videoStr ;
    UIView *subView ;

}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgHeightLayout;
@property (strong, nonatomic) IBOutlet UIButton *moreBtn;
- (IBAction)moreClicked:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
- (IBAction)downloadCClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtThemeCollection;
@property (strong, nonatomic) IBOutlet UILabel *txtThemeNamee;
@property (strong, nonatomic) IBOutlet UIImageView *imgPhotos;
@property (strong, nonatomic) IBOutlet UIImageView *imgInfo;
- (IBAction)downloadClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UILabel *txtArtist;
@property (strong, nonatomic) IBOutlet UILabel *txtThemeName;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *downloadTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *swipableTop;
@property (strong, nonatomic) IBOutlet UIScrollView *scllView;
@property (strong, nonatomic) IBOutlet UILabel *Time_lbe;
@property (strong, nonatomic) IBOutlet UILabel *Weekday_lbe;
@property (strong, nonatomic) IBOutlet UIButton *back_btn;
- (IBAction)Back_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Save_btn;
@property (strong, nonatomic) IBOutlet UIImageView *imgBig;
@property (nonatomic) BOOL isPresentBtn;
@property(strong,nonatomic)UIImage *imageName;
@property (strong, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)homeClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *themesClicked;
- (IBAction)themesClciked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (strong, nonatomic) IBOutlet UIView *swipableView;
@property (strong, nonatomic) IBOutlet UIView *movableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movableViewBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movableViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movableTop;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnDownloadHeigtht;
@property (strong, nonatomic) IBOutlet UIButton *btnDownloadSecond;
@property (strong, nonatomic) IBOutlet UIImageView *imgGradient;
@property(strong,nonatomic)NSString *collectionStr ;
@property(strong,nonatomic)NSString *descriptionStr ;
@property(strong,nonatomic)NSString *idStr ;
@property(strong,nonatomic)NSString *nameStr ;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightLayoutView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *DROPiMG;
@property (strong, nonatomic) IBOutlet UIImageView *grayBorder;

@property (strong, nonatomic) AVPlayer * av_player;
@property (strong, nonatomic) AVPlayerLayer * av_layer;
@property(strong,nonatomic)NSString *VideoNameStr ;

@property (strong, nonatomic) IBOutlet UIView *ViewBorder;

@end
