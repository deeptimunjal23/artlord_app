//
//  main.m
//  MobileImage
//
//  Created by Anil on 14/10/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
