


#import "ViewController.h"
#import "InfoController.h"
#import "MyThemesController.h"
#import "PointsViewController.h"
#import "Reachability.h"
#import "KeychainItemWrapper.h"
#import "HelpVC.h"
#import "AppDelegate.h"
#import "customFreePoints.h"
#import "AppDelegate.h"
#import <DFContinuousForceTouchGestureRecognizer/DFContinuousForceTouchGestureRecognizer.h>
#import <Photos/Photos.h>
#import "MobileImage-Swift.h"

#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)
@interface ViewController ()<UIViewControllerPreviewingDelegate,DFContinuousForceTouchDelegate>{
    int rightIndex;
    int leftIndex;
    NSString *stringImage;
    BOOL goMyThemView;

}

@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic) DFContinuousForceTouchGestureRecognizer* imageForceTouchRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer* tapGestureRecognizer;
@property(readwrite, nonatomic, strong) LivePhoto *livePhoto;

@end



@implementation ViewController

- (void)viewDidLoad {
    
//    self.slidingView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.9];
    
    PHAsset *asset = [PHAsset fetchAssetsWithALAssetURLs:fileURL options:nil];

    _TxtThemeAdded.adjustsFontSizeToFitWidth = YES;
    _TxtThemeAdded.minimumFontSize = 7;
    

   
    
     self.crimpsonFirstView.hidden =  YES ;
    
    closeSection = true ;
    
    collectionsArray = [[NSMutableArray alloc]init];
    
    
    _txtTap.hidden = YES;
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    _btnCloseTable.layer.cornerRadius = _btnCloseTable.bounds.size.width/2;
//    [_btnCloseTable setClipsToBounds:YES];
//    _btnCloseTable.layer.borderWidth = 1;
//    _btnCloseTable.layer.borderColor = [UIColor grayColor].CGColor;

    arrayAdd = [NSMutableArray new];
    
    // [self getAllThemesDataFromServer];
    
    [avPlayer seekToTime:CMTimeMake(0, 1)];
    [avPlayer pause];
    [self.btnTheme setSelected:NO];
    
    if ([UIScreen mainScreen].bounds.size.width == 320) {
        
        _viewLeading.constant = -295 ;
        _viewWidth.constant = -295;
        x = 10;
        _menuBtnLeading.constant = x ;
        
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:avPlayer];
    
    self.slider.hidden = YES;
    
    _slider.userInteractionEnabled = NO ;
    
    
    _landscapeSlider.hidden = YES;
    _bottomViewSecond.hidden = YES;
     _bottomView.hidden = YES;
    

    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    
    
    collectionName = [[NSMutableArray alloc]init];
    
  //  collectionName = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"collectionName"]];
    
    
    
    NSString*str = [SSKeychain passwordForService:@"collectionName" account:@"collectionName"];
    if (str!=nil) {
        
         [collectionName addObject:[str componentsSeparatedByString:@","]];
    }
    
   

    
    
    self.imagesScrollView.scrollEnabled = YES;
    
    user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"User_id"];
    NSLog(@"%@",user_id);
    
    
    rightDraggingCount = 0 ;
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setInteger:rightDraggingCount forKey:@"rightCount"];
    [userdefault synchronize];
    
    
    
    //    [[NSUserDefaults standardUserDefaults]setInteger:rightDraggingCount forKey:@"rightCount"];
    //    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    _slider.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
    
    _slider.value =0.0 ;
    _slider.hidden = YES;
    
    
    _landscapeSlider.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
    
    _landscapeSlider.value =0.0 ;
    _landscapeSlider.hidden = YES;
    
    
    self.AdView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.8];
    
    self.AdView.hidden = YES;
    self.lblSeventh.hidden = YES;
    self.lblSixth.hidden = YES;
    self.lblFifth.hidden = YES;
    self.leftLblFirst.hidden = YES;
    self.rightLabelFirst.hidden = YES;
    self.lblFourth.hidden = YES;
    
    [_btnTheme setHidden:YES];
    
    [self dottBtn];
    
    self.viewRed.backgroundColor = [UIColor clearColor];
    self.viewRed.layer.cornerRadius = self.viewRed.frame.size.width/2;
    [self.viewRed clipsToBounds];
    self.viewRed.backgroundColor = [UIColor redColor];
    self.viewRed.layer.borderWidth = 1.8;
    self.viewRed.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.dottedView.backgroundColor = [UIColor clearColor];
    self.subsubView.layer.cornerRadius = self.subsubView.frame.size.width/2;
    [self.subsubView clipsToBounds];
    self.subsubView.backgroundColor = [UIColor clearColor];
    self.subsubView.layer.borderWidth = 2.7;
    self.subsubView.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.subView.layer.cornerRadius = self.subView.frame.size.width/2;
    [self.subView clipsToBounds];
    self.subView.backgroundColor = [UIColor clearColor];
    self.subView.layer.borderWidth = 2.0;
    self.subView.layer.borderColor = [UIColor grayColor].CGColor;
    self.subView.backgroundColor = [UIColor clearColor];
    
    _imagesScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(-1, 0,  self.view.frame.size.width +1,  self.view.frame.size.height)];
    
    keys = [[NSMutableArray alloc]init];
  // delegate.featuredkeys = [[NSMutableArray alloc]init];
    
    self.viewInfoThemeSave.hidden = YES;
    _strKey =  @"Live";
   // delegate.imagesArray = [[NSMutableArray alloc]init];
    imagesSortingArray = [[NSMutableArray alloc]init];
    
    description =  [[NSMutableArray alloc]init];
    name =  [[NSMutableArray alloc]init];
    collection =  [[NSMutableArray alloc]init];
    idd = [[NSMutableArray alloc]init];
    
    _viewMenuSettings.layer.borderColor= [UIColor darkGrayColor].CGColor;
    _viewMenuSettings.layer.borderWidth=2.0f;
    
    
    _btnTheme.layer.cornerRadius = 5.0f;
    _info_Btn.layer.cornerRadius = 5.0f;
    _info_Btn.clipsToBounds= YES;
    _btnTheme.clipsToBounds= YES;
    
    
    
    _txtCoinsSave.text = @"100";
    _txtLbl.text = @"100";
    strCoinsUsed = [[NSString alloc]init];
    
    [self.ViewSaved setHidden:YES];
    
    
    
    arr1=[[NSMutableArray alloc]init];
    
  
    
    
    
    [self addPortraitLandscapeScrollView];
    
    
    [self.view addSubview:_imagesScrollView];
    _imagesScrollView.delegate = self;
    [self.view sendSubviewToBack:_imagesScrollView];
    
    
    [self.view bringSubviewToFront:_info_Btn];
    [self.view sendSubviewToBack:_scrllView];
    [_imagesScrollView setPagingEnabled:YES];
    [_imagesScrollView setAlwaysBounceVertical:NO];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(layoutBtns) name:@"DeepLink" object:nil];
    [self layoutBtns];
    
    
    
    [self.view bringSubviewToFront:_info_view];
    _swipableView.alpha = 0;
    
    _bottomView.backgroundColor = [UIColor clearColor];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.info_view.hidden =YES;
    
    
    
    
    _imageView.multipleTouchEnabled = YES;
    [ _imageView canBecomeFirstResponder];
    _imageView.userInteractionEnabled = YES;
    
    freeimg=[NSMutableArray arrayWithObjects:@"Layer 11.png",@"Layer 16.png",@"Layer 14 (1).png",@"Layer 17.png",@"Layer 13 copy 3.png",@"Layer 18.png",nil];
    
    _currentVideoTime.hidden = YES;
    
    
    [self getThemePointsFromServer];
    
    
  //  [self.view bringSubviewToFront:self.txtTap];

}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender
{
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     NSString*str1 = delegate.imagesArray[pageNo];
    if ([sender isEqual:self.lpgr]) {
        if (sender.state == UIGestureRecognizerStateBegan)
        {
                        
            if ([str1 containsString:@"mp4"])
            {
                
                [avPlayer setAllowsExternalPlayback:YES];
                
                [avPlayer seekToTime:CMTimeMake(0, 1)];
                
                
                avPlayer = (AVPlayer*)[avPlayerArray objectAtIndex:pageNo];
                NSLog(@"%@",avPlayerArray);
                [avPlayer play];
                
                
                [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
                
                currentItem = avPlayer.currentItem;
                
                
                NSTimeInterval totalTime = CMTimeGetSeconds(currentItem.duration);
                
                _slider.maximumValue = totalTime;
                _landscapeSlider.maximumValue = totalTime;
                
            }
            else{
                
                
            }
           


            
        }
        else if(sender.state == UIGestureRecognizerStateEnded){
            
            if ([str1 containsString:@"mp4"])
            {

                [avPlayer seekToTime:CMTimeMake(0, 1)];
                [avPlayer pause];
                
                
                [avPlayer setAllowsExternalPlayback:YES];
            }else{
                
                
            }
            
           
            
            
        }
        
        
        NSString*str1 = delegate.imagesArray[pageNo];
        
        
        if ([str1 containsString:@"mp4"]) {
            [_btnTheme setHidden:YES];
            self.slider.hidden = NO;
            self.landscapeSlider.hidden = YES ;
            
            
        }else{
            [_btnTheme setHidden:YES];
            
            self.slider.hidden = YES;
            self.landscapeSlider.hidden = YES ;
        }
        
        
        if(isPortrait== YES){
            
            self.slider.hidden = NO;
            self.landscapeSlider.hidden = YES;
            
        }else{
            self.slider.hidden = YES;
            self.landscapeSlider.hidden = NO;
            
        }

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void) orientationChanged:(NSNotification *)note
{
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    deepLink = NO ;
    
    
    UIDevice * device = note.object;
    switch(device.orientation)
    {
            
        case UIDeviceOrientationPortrait:
            
            isPortrait = YES;
            
            
            _slider.value = 0.0 ;
            _landscapeSlider.value = 0.0 ;
            
            
            
            isPortrait = true ;
            
            
            _sliderTop.constant  = 2 ;
            _imagesScrollView.frame = CGRectMake(-1, 0,  self.view.frame.size.width +1,  self.view.frame.size.height);
            _imagesScrollView.scrollEnabled = YES;
            if (![_btnMenu isSelected]) {
                _btnMenu.hidden = NO ;
            }
            
            _viewRed.hidden = YES;
            _btnCoins.hidden = NO ;
            
            _btnLandscapeCoins.hidden = YES;
            _imgLock.hidden = YES;
            
            _txtSwipe.hidden = NO;
            _txtTap.hidden = NO;
            
            [_themeNamee setFont: [_themeNamee.font fontWithSize: 32]];
            [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 20]];
            [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 32]];
            
            _widthCoinLayout.constant = 40 ;
            _imgwidthLayout.constant = 60 ;
            _yLayout.constant = 1.53;
            
            [_lbe_Time.layer removeAllAnimations];
            [_lbe_Weekdays.layer removeAllAnimations];
            _bottomViewSecond.hidden = YES;
             _bottomView.hidden = YES;
            
            _playLayoutTrailing.constant = 10;
            _slider.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
            
            
            //            _lbe_Time.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
            //            _lbe_Weekdays.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
            _btnTheme.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
            [self.view bringSubviewToFront:self.btnMenu];
            
            if (delegate.imagesArray.count > 0) {
                if (pageNo!= (int)nil) {
                    videoStr = delegate.imagesArray[pageNo];
                    NSLog(@"Page NUmber %d",pageNo);
                    NSLog(@"Video Str %@",videoStr);
                }else{
                    
                    videoStr = delegate.imagesArray[0];
                }
                
                
            }
            else {
                _txtTap.hidden = true;
                _txtSwipe.hidden = true;
            }
            
            if ([videoStr containsString:@"mp4"]) {
                
                _slider.hidden = NO;
                _landscapeSlider.hidden = YES;
                
            }else{
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = YES;
            }


            // _txtTap.hidden = NO;
            
            
            break;
            
        case UIDeviceOrientationLandscapeRight:
            
            
             isPortrait = NO;
            // _sliderTop.constant  = -15 ;
            _imagesScrollView.frame = CGRectMake(-1, 0, self.view.frame.size.width, self.view.frame.size.height+1);
            _imagesScrollView.scrollEnabled = NO;
            _btnMenu.hidden = YES ;
            _viewRed.hidden = YES;
            _btnCoins.hidden = YES;
            _btnLandscapeCoins.hidden = NO;
            _imgLock.hidden = NO;
            
            _txtSwipe.hidden = YES;
            _txtTap.hidden = YES;
            [_themeNamee setFont: [_themeNamee.font fontWithSize: 24]];
            [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 14]];
            [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 24]];
            _widthCoinLayout.constant = 30 ;
            _imgwidthLayout.constant = 35 ;
            _yLayout.constant = 5.0;
            
            [self.view sendSubviewToBack:_imagesScrollView];
            _btnLandscapeCoins.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            
            _imgLock.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _bottomViewSecond.hidden = YES;
            _bottomView.hidden = YES;
            
            _bottomViewSecond.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            
            _slider.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            
            _btnTheme.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _playLayoutTrailing.constant =   self.view.frame.size.width-42 ;
            
            _landscapeSlider.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _landscapeSliderX.constant = -self.view.frame.size.width/2 +2 ;
            
            if ([UIScreen mainScreen].bounds.size.width == 320) {
                _bottomViewX.constant = 140 ;
            } else{
                _bottomViewX.constant = 170 ;
            }
            
            
            if (delegate.imagesArray.count > 0) {
                if (pageNo!= (int)nil) {
                    videoStr = delegate.imagesArray[pageNo];
                    NSLog(@"Page NUmber %d",pageNo);
                }else{
                    
                    videoStr = delegate.imagesArray[0];
                }
            }
            else {
                _txtTap.hidden = true;
                _txtSwipe.hidden = true;
            }

            NSLog(@"Video Str %@",videoStr);
            if ([videoStr containsString:@"mp4"]) {
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = NO;
                
            }else{
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = YES;
            }

           
            
            [self.view layoutIfNeeded];
//             _txtTap.hidden = YES;
            
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            
            _txtTap.hidden = YES;
            
            
             isPortrait = NO;
            _slider.value = 0.0 ;
            _landscapeSlider.value = 0.0 ;
            
            _sliderTop.constant  = -15;
            _imagesScrollView.frame = CGRectMake(-1, 0, self.view.frame.size.width, self.view.frame.size.height+1);
            
            _imagesScrollView.scrollEnabled = NO;
            _btnMenu.hidden = YES ;
            _viewRed.hidden = YES;
            _btnCoins.hidden = YES ;
            _btnLandscapeCoins.hidden = NO;
            _imgLock.hidden = NO;
            
            _txtSwipe.hidden = YES;
            
            
            
            [_themeNamee setFont: [_themeNamee.font fontWithSize: 24]];
            [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 16]];
            [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 24]];
            
            _widthCoinLayout.constant = 30 ;
            _imgwidthLayout.constant = 35 ;
            _yLayout.constant = 5.0;
            
            [self.view sendSubviewToBack:_imagesScrollView];
            
            
            _btnLandscapeCoins.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) /-180 );
            _imgLock.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _bottomViewSecond.hidden = YES;
            _bottomView.hidden = YES;
            _bottomViewSecond.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            
            _btnTheme.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _slider.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            
            _playLayoutTrailing.constant =   self.view.frame.size.width-42 ;
            _landscapeSlider.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / -180 );
            _landscapeSliderX.constant = -self.view.frame.size.width/2 + 2;
            
            
            if ([UIScreen mainScreen].bounds.size.height == 320) {
                _bottomViewX.constant = 140 ;
            } else{
                _bottomViewX.constant = 160 ;
            }
            [self.view layoutIfNeeded];
            
            
            if (delegate.imagesArray.count > 0) {
                if (pageNo!= (int)nil) {
                    videoStr = delegate.imagesArray[pageNo];
                    NSLog(@"Page NUmber %d",pageNo);
                }else{
                    
                    videoStr = delegate.imagesArray[0];
                }
            }
            else {
                _txtTap.hidden = true;
                _txtSwipe.hidden = true;
            }
            

            NSLog(@"Video Str %@",videoStr);
            if ([videoStr containsString:@"mp4"]) {
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = NO;
                
            }else{
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = YES;
            }

          
            
            break;
            
            
        default:
            break;
    };
    
    
    
   
//    
//    [_imagesScrollView setContentOffset:CGPointMake(_imagesScrollView.frame.size.width*pageNo, 0)];
//    
//    [self.view addSubview:_imagesScrollView];
//    
//    [self scrollimages];
//    
//    
//    
//    [self.btnMenu bringSubviewToFront:self.btnMenu];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    
//    _btnCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//    self.btnCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;
//    NSString *fontName = self.btnCoins.titleLabel.font.fontName;
//    CGFloat fontSize = self.btnCoins.titleLabel.font.pointSize;
//    _btnLandscapeCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//    self.btnLandscapeCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;

    _txtCoinsCount.adjustsFontSizeToFitWidth = YES;
    self.txtCoinsCount.lineBreakMode = NSLineBreakByClipping;

  //  NSString *countSavedImages =  [[NSUserDefaults standardUserDefaults]objectForKey:@"SavedCollectionCount"];
    
    NSString *countSavedImages = [NSString stringWithFormat:@"%lu",(unsigned long)delegate.myCollectionArray.count];
   
    NSLog(@"%@",countSavedImages);
    NSString *SavedThemeStr;
    if (countSavedImages!=nil) {
        SavedThemeStr = [NSString stringWithFormat:@"My Collection (%@)",countSavedImages];
    }
    else{
        
        countSavedImages = @"0";
        SavedThemeStr = [NSString stringWithFormat:@"My Collection (%@)",countSavedImages];
        
        
    }
    
    
    
    //    menuItemsArray = [[NSMutableArray alloc]initWithObjects:SavedThemeStr, @"Featured",@"Environment",@"Creature",@"Fantasy",@"Digital Art",@"Landscape",@"Illustration",@"Science Fiction",@"Industrial Design",@"Help Topics",nil];
    
    
    menuItemsArray = [[NSMutableArray alloc]initWithObjects:SavedThemeStr,@"Live Screens",@"Featured",@"The Crimson Carrot",@"Collections", nil];
    
    
    [self CategoryDataFromServer];
    
    
    self.viewRed .hidden = YES;
    
    [self.menuTableview reloadData];
    
    self.viewThird.hidden = YES;
    
    saveClicked = NO;
    
    [self getImagesFromServer];
    
    NSString *coins = [SSKeychain passwordForService:@"image" account:@"imagelordData"];
    NSLog(@"%@",coins);
    
    int coinsCount = [coins intValue] ;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == false)
    {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"keychain"] == false){
             self.txtTap.hidden = YES;
            
//            animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//            [animation setFromValue:[NSNumber numberWithFloat:0.0]];
//            [animation setToValue:[NSNumber numberWithFloat:2.0]];
//            [animation setDuration:.5f];
//            [animation setRepeatCount:100];
//            [animation setAutoreverses:YES];
//            [animation setRemovedOnCompletion:NO];
//            [self.txtTap.layer addAnimation:animation forKey:@"animation"];  [super viewDidLoad];

            
            if (coinsCount > 100) {
                self.viewFirst.hidden = NO;
                self.viewSecond.hidden = YES;
                self.viewThird.hidden = YES;
                self.viewFourth.hidden = YES;
                self.viewFive.hidden = YES;
                self.viewSix.hidden = YES;
                self.viewSeven.hidden = YES;
                self.viewFirstTime.hidden = NO;
                self.leftLblFirst.hidden = NO;
                self.rightLabelFirst.hidden = NO;
                
            }
            else if(coins.length==0 || [coins isKindOfClass:[NSNull class]] || [coins isEqualToString:@""]||[coins isEqualToString:@"(null)"]||coins==nil || [coins isEqualToString:@"<null>"]){
                self.viewFirst.hidden = NO;
                self.viewSecond.hidden = YES;
                self.viewThird.hidden = YES;
                self.viewFourth.hidden = YES;
                self.viewFive.hidden = YES;
                self.viewSix.hidden = YES;
                self.viewSeven.hidden = YES;
                self.viewFirstTime.hidden = NO;
                self.leftLblFirst.hidden = NO;
                self.rightLabelFirst.hidden = NO;
                
               
                
                
                
            }
            else
            {
                [self.viewFirstTime setHidden:YES];
                
                 self.txtTap.hidden = NO;
                
            }
            
        }else{
            //  [self.viewFirstTime setHidden:YES];
            
            
        }
        
    }
    
    
    
    
   // keys = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"Keys"]];
    
    
    NSString*str = [SSKeychain passwordForService:@"keys" account:@"keys"];
    
    if (str!= nil) {
        [keys addObject:[str componentsSeparatedByString:@","]];

    }
    
    _info_Btn.hidden = YES;
    // _btnTheme.hidden = NO;
    _btnTheme.hidden = YES;
    
    _lbe_Time.hidden = NO;
    _lbe_Weekdays.hidden = NO;
     _bottomView.hidden = YES;
    _txtSwipe.hidden = NO;
    
    self.viewNoThemes.hidden = YES;
    
    _saveBtn.alpha = 1.0;
    _txtCoinsSave.alpha = 1.0;
    
    if(delegate.isNoRecord){
        
        _info_Btn.hidden = YES;
        _saveBtn.alpha = 0.0;
        _txtCoinsSave.alpha = 0.0;
        
        _lbe_Time.hidden = YES;
        _lbe_Weekdays.hidden = YES;
        
        _txtSwipe.hidden = YES;
        
        self.viewNoThemes.hidden = NO;
        
        [self.view bringSubviewToFront:self.btnMenu];
        [self.view bringSubviewToFront:_btnCoins];
        [self.view bringSubviewToFront:_btnLandscapeCoins];
        [self.view bringSubviewToFront:_btnTheme];
        [self.view bringSubviewToFront:_bottomView];
        // _btnTheme.hidden = NO;
        _btnTheme.hidden = YES;
        
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"]==false) {
        [_btnCoins setTitle:@"250" forState:UIControlStateNormal];
        [_btnLandscapeCoins setTitle:@"250" forState:UIControlStateNormal];
        CGFloat fontSize = self.btnCoins.titleLabel.font.pointSize;
        NSString *fontName = self.btnCoins.titleLabel.font.fontName;
//        self.btnCoins.titleLabel.font = [UIFont systemFontOfSize:fontSize-2];
//        _btnCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//        self.btnCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;
//        _btnLandscapeCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//        self.btnLandscapeCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;

        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
        
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:_btnCoins.titleLabel.text  forKey:@"coins"];
        [userdefault synchronize];
        
        NSUserDefaults *userdefault2 = [NSUserDefaults standardUserDefaults];
        [userdefault2 setObject:_btnLandscapeCoins.titleLabel.text  forKey:@"coins"];
        [userdefault2 synchronize];
        
        
    }else{
        
        NSString *strKeychain =[SSKeychain passwordForService:@"image" account:@"imagelordData"];
        
        NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"coins"]];
        
        if (strKeychain!=nil && strKeychain!=(id)[NSNull null]) {
            if (![strKeychain isEqualToString:@"(null)"]) {
                [SSKeychain setPassword:strKeychain forService:@"image" account:@"imagelordData"];

                [self.btnCoins setTitle:strKeychain forState:UIControlStateNormal];
//                if (strKeychain.length >3) {
//                    CGFloat fontSize = self.btnCoins.titleLabel.font.pointSize;
//                    self.btnCoins.titleLabel.font = [UIFont systemFontOfSize:fontSize-2];
//                }
                [self.btnLandscapeCoins setTitle:strKeychain forState:UIControlStateNormal];
                
//                _btnCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//                _btnLandscapeCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//                self.btnCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;
//                self.btnLandscapeCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;

                _txtCoinsCount.text = self.btnCoins.titleLabel.text ;
            }
            
        }
        
    }
    
    
    NSString *coinsValue  = _btnCoins.titleLabel.text ;
    
    [_btnClose setHidden:YES];
    _imgGradient.alpha = 0;
    
    [self.view bringSubviewToFront:_info_view];
    
    
    
    
    [self scrollimages];
    
    
    [self.view bringSubviewToFront:self.slidingView];
    
    self.themesVieww.backgroundColor = [UIColor colorWithRed:10/255.0f green:8/255.0f blue:7/255.0f alpha:0.9];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO]; // or NO to disable rotation
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"specificTheme"]==true) {
        
        
        
        NSUInteger indexOfTheObject = [[NSUserDefaults standardUserDefaults]integerForKey:@"SpecificThemeCount"];
        
        [_imagesScrollView setContentOffset:CGPointMake(_imagesScrollView.frame.size.width*indexOfTheObject, 0)];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"specificTheme"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        self.viewNoThemes.hidden = YES;
        
        deepLink = YES;
        
        [self.view sendSubviewToBack:self.imagesScrollView];
        
        
        
        

        
    }
    else{
        deepLink = NO;
        // isPortrait = NO;
        
        [self.view sendSubviewToBack:self.viewNoThemes];
        
        
        
    }
    
    NSString *str1;
    
    if (delegate.imagesArray.count>0) {
        if (pageNo!=(int)nil) {
            str1 = delegate.imagesArray[pageNo];
        }
        else{
            if (delegate.imagesArray.count >0) {
                str1 = delegate.imagesArray[0];
            }
            
        }

    }else
    {
        
        [self noThemesMethod];
        
    }
    
    
    if ([str1 containsString:@"mp4"]) {
        [_btnTheme setHidden:YES];
        isPortrait = YES;
        _txtTap.hidden = NO;
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setFromValue:[NSNumber numberWithFloat:0.0]];
        [animation setToValue:[NSNumber numberWithFloat:2.0]];
        [animation setDuration:.5f];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        [animation setRemovedOnCompletion:NO];
        [self.btnTheme.layer addAnimation:animation forKey:@"animation"];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == false)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"keychain"] == false){
                 _txtTap.hidden = YES;
                
            }else{
                
                
            }
      
            
        }else{
             _txtTap.hidden = NO;
            
        }
       
        
        
    }else{
        
        [_btnTheme setHidden:YES];
        isPortrait = NO;
        _txtTap.hidden = YES;
        
    }
    
   // [self check3DTouch];
     BOOL isTouch = [[self.imagesScrollView traitCollection] forceTouchCapability] == UIForceTouchCapabilityAvailable;
  
//    if ([self.traitCollection respondsToSelector:@selector(forceTouchCapability)] &&
//        self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable) {
//        // This won't get called because forceTouchCapability is returning nil
//        // which corresponds to UIForceTouchCapabilityUnknown
//        [self registerForPreviewingWithDelegate:self sourceView:self.view];
//    }
//    
    if (isTouch) {
        _imageForceTouchRecognizer = [[DFContinuousForceTouchGestureRecognizer alloc] init];
//        _imageForceTouchRecognizer.timeout = 0.5f;
//        _imageForceTouchRecognizer.forceTouchDelay = 0.2f;
//        _imageForceTouchRecognizer.baseForceTouchPressure = 3.0f;
//        _imageForceTouchRecognizer.triggeringForceTouchPressure = 6.0f;
        _imageForceTouchRecognizer.forceTouchDelegate = self;
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self.imagesScrollView addGestureRecognizer:_tapGestureRecognizer];
        [self.imagesScrollView addGestureRecognizer:_imageForceTouchRecognizer];
    }
    else
    {
        self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
        self.lpgr.delegate = self;
        self.lpgr.enabled = YES;
        self.lpgr.minimumPressDuration = 0.1f;
        self.lpgr.allowableMovement = 100.0f;
    
        [self.imagesScrollView addGestureRecognizer:self.lpgr];

    }
    self.menuTableview.separatorStyle = UITableViewCellSeparatorStyleNone ;
    
    
}
#pragma mark -
#pragma UITapGestureRecognizer target selector

- (void) tapped:(id)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"3D Touch");
    });
}
#pragma mark -
#pragma UIContinuousForceTouchDelegate
- (void) forceTouchDidTimeout:(DFContinuousForceTouchGestureRecognizer*)recognizer{
    NSLog(@"touch time out");
}
//required
- (void) forceTouchRecognized:(DFContinuousForceTouchGestureRecognizer*)recognizer {
   

    if(recognizer == _imageForceTouchRecognizer)
    {
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        NSString*str1 = delegate.imagesArray[pageNo];
        if ([str1 containsString:@"mp4"])
        {
            [avPlayer setAllowsExternalPlayback:YES];
            
            [avPlayer seekToTime:CMTimeMake(0, 1)];
            
            
            avPlayer = (AVPlayer*)[avPlayerArray objectAtIndex:pageNo];
            NSLog(@"%@",avPlayerArray);
            [avPlayer play];
            
            
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
            
            currentItem = avPlayer.currentItem;
            
            
            NSTimeInterval totalTime = CMTimeGetSeconds(currentItem.duration);
            
            _slider.maximumValue = totalTime;
            _landscapeSlider.maximumValue = totalTime;

        }
        
            //            [self.av_player setAllowsExternalPlayback:YES];
            //
            //            [self.av_player seekToTime:CMTimeMake(0, 1)];
            //
            //
            //            [self.av_player play];
            
            } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        });
    }

}
- (void) forceTouchRecognizer:(DFContinuousForceTouchGestureRecognizer*)recognizer didEndWithForce:(CGFloat)force maxForce:(CGFloat)maxForce  {
    
    if(recognizer == _imageForceTouchRecognizer) {
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSString*str1 = delegate.imagesArray[pageNo];
        if ([str1 containsString:@"mp4"])
        {
            avPlayer = (AVPlayer*)[avPlayerArray objectAtIndex:pageNo];
            NSLog(@"%@",avPlayerArray);
            [avPlayer pause];
            [self.av_player seekToTime:CMTimeMake(0, 1)];
            [self.av_player pause];
            [self.av_player setAllowsExternalPlayback:YES];
        }
        
    }else{
        
        
    }
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    
    if ([self.traitCollection respondsToSelector:@selector(forceTouchCapability)]) {
        if (self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable) {
            // retain the context to avoid registering more than once
           
        } else {
        }
    }
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}


-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    if ([UIScreen mainScreen].bounds.size.width == 320) {
        self.viewFourthTop.constant = -40 ;
        [self.view layoutIfNeeded];
        
        
    }else{
        self.viewFourthTop.constant = -10 ;
        [self.view layoutIfNeeded];
        
    }
    
    
    if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
    {
        
        if ([UIScreen mainScreen].bounds.size.height==480) {
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        } if ([UIScreen mainScreen].bounds.size.height==568) {
            _infoViewTop.constant = -40;
            [self.view layoutIfNeeded];
            
        }else{
            _infoViewTop.constant = 0;
            [self.view layoutIfNeeded];
            
            
            
        }
        
    }else if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft)){
        if ([UIScreen mainScreen].bounds.size.height==480) {
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        }else if ([UIScreen mainScreen].bounds.size.height==568) {
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        }else{
            
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        }
    }
    
    else if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight)){
        
        if ([UIScreen mainScreen].bounds.size.height==480) {
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        }else if ([UIScreen mainScreen].bounds.size.height==568) {
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
        }else{
            
            _infoViewTop.constant = -110;
            [self.view layoutIfNeeded];
            
            
        }
    }
    
    CGFloat fixedWidth = _txtView.frame.size.width;
    CGSize newSize = [_txtView sizeThatFits:CGSizeMake(fixedWidth, 800)];
    CGRect newFrame = _txtView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    _txtView.frame = newFrame;
    
    float shadowSize = 10.0f;
    shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(_themesVieww.frame.origin.x - shadowSize / 2,
                                                             _themesVieww.frame.origin.y - shadowSize / 2,
                                                             _themesVieww.frame.size.width + shadowSize,
                                                             _themesVieww.frame.size.height + shadowSize)];
    
    
    
    
    
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    [self.btnTheme setBackgroundImage:[UIImage imageNamed:@"livephoto-icon.png"] forState:UIControlStateNormal];
    
    [avPlayer seekToTime:CMTimeMake(0, 1)];
    [avPlayer pause];
    [self.btnTheme setSelected:NO];
    
    
    
    
}





#pragma mark
#pragma Used for ImagesScroling
#pragma mark

-(void)scrollimages{
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    NSLog(@"%@",self.imagesScrollView.subviews);
    if (self.imagesScrollView.subviews.count >0)
    {
        [self.imagesScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
    }
    
    
    
    
    NSLog(@"%@",self.imagesScrollView.subviews);
    
    avPlayerArray = [NSMutableArray new];
    sliderArray = [NSMutableArray new];
    
    
    
    for (int i = 0; i < [delegate.imagesArray count]; i++)
    {
        
        
        AVPlayer  *audioPlayer = [[AVPlayer alloc]init];
        
        
        
        xOrigin = i * _imagesScrollView.frame.size.width;
        
        NSString *str  = [delegate.imagesArray objectAtIndex:i];
        
        
        count = 0;
        
        if ([str containsString:@"mp4"]) {
            
            
           // self.slider.hidden = NO;
            videoStr  = [delegate.imagesArray objectAtIndex:i];
            NSLog(@"Video Index %d",i);
            NSLog(@"Video Str %@",videoStr);
            
            self.btnTheme.userInteractionEnabled = YES;
            
            NSArray* foo = [videoStr componentsSeparatedByString: @"."];
            NSString* namee = [foo objectAtIndex: 0];
            
            NSString*type = [foo objectAtIndex:1];
            NSString *filepath =[[NSBundle mainBundle] pathForResource: namee ofType:type inDirectory:nil];
            fileURL =   [NSURL fileURLWithPath:filepath];
            
            // NSURL *fileURL = [[NSBundle mainBundle] URLForResource:videoStr withExtension:nil];
            
            
            audioPlayer = [AVPlayer playerWithURL:fileURL];
            
            self.av_layer = [AVPlayerLayer layer];
            
            [self.av_layer setPlayer:audioPlayer];
            
            self.av_layer.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height);
            [self.av_layer setVideoGravity:AVLayerVideoGravityResizeAspect];
            
            UIView *subViews = [[UIView alloc]init];
            
            subViews.frame =  CGRectMake(xOrigin, 0,[[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            [subViews.layer addSublayer:_av_layer];
            
            [_imagesScrollView addSubview:subViews];
            
            
            
            self.imageView.backgroundColor = [UIColor clearColor];
            self.view.backgroundColor = [UIColor blackColor];
            
            
            [avPlayerArray addObject:audioPlayer];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == false)
            {
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"keychain"] == false){
                    _txtTap.hidden = YES;
                    
                }
                
                
            }else{
                _txtTap.hidden = NO;
                
            }

            
           

            
        }else{
            
            _txtTap.hidden = YES;

            
            //            [_btnTheme setHidden:YES];
            
            imageView.contentMode = UIViewContentModeScaleToFill;
            
            imageView.backgroundColor = [UIColor blackColor];
            
            if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft)  )
            {
                
                //  [self rotationToLandscape];
                [self.imagesScrollView setScrollEnabled:NO];
                
                
            }
            else if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight))
            {
                
                // [self rotationToLandscape];
                
                [self.imagesScrollView setScrollEnabled:NO];
                
            }
            else{
                [self.imagesScrollView setScrollEnabled:YES];
                
            }
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, _imagesScrollView.frame.size.width+1, _imagesScrollView.frame.size.height)];
            
            [imageView setImage:[UIImage imageNamed:[delegate.imagesArray objectAtIndex:i]]];
            [imageView removeFromSuperview];
            
            
            [_imagesScrollView addSubview:imageView];
            
            [self.view sendSubviewToBack:_imagesScrollView];
            
            [avPlayerArray addObject:@""];
        }
        
        
        
        
    }
    
    
    
    
    
    [_imagesScrollView setContentSize:CGSizeMake(_imagesScrollView.frame.size.width * [delegate.imagesArray count], _imagesScrollView.frame.size.height)];
    
    
}


-(void)rotationToLandscape{
    [self.imagesScrollView setScrollEnabled:NO];
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([UIScreen mainScreen].bounds.size.width == 568) {
        
        
        
        if (pageNo == delegate.imagesArray.count -1) {
            CGRect fr = imageView.frame ;
            fr.origin.x = xOrigin ;
            fr.origin.y = 0 ;
            imageView.frame = fr;
        }else{
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin + 125, -125, _imagesScrollView.frame.size.height, _imagesScrollView.frame.size.width+1)];
            
            
            imageView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
            
            
        }
        
    }
    
    else if ([UIScreen mainScreen].bounds.size.width == 667) {
        
        if (pageNo == delegate.imagesArray.count -1) {
            CGRect fr = imageView.frame ;
            fr.origin.x = xOrigin ;
            fr.origin.y = 0 ;
            imageView.frame = fr;
        }else{
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin + 146, -146, _imagesScrollView.frame.size.height, _imagesScrollView.frame.size.width+1)];
            
            imageView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
            
            
        }
    }
    else{
        
        if (pageNo == delegate.imagesArray.count -1) {
            CGRect fr = imageView.frame ;
            fr.origin.x = xOrigin ;
            fr.origin.y = 0 ;
            imageView.frame = fr;
        }else{
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin + 162, -162, _imagesScrollView.frame.size.height, _imagesScrollView.frame.size.width+1)];
            
            imageView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
            
            
            
            
        }
        
    }
}



#pragma mark
#pragma Time update Method
#pragma mark


- (void)updateTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    if (showBlinkTimer) {
        [formatter setDateFormat:@"h mm"];
        showBlinkTimer = false;
        
    }
    else {
        [formatter setDateFormat:@"h:mm"];
        showBlinkTimer = true;
        
    }
    NSString *timeString = [formatter stringFromDate:[NSDate date]];
    
    self.lbe_Time.text = timeString;
    
    formatter.dateFormat=@"dd";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    
    formatter.dateFormat=@"MMMM";
    NSString *monthString = [[formatter stringFromDate:[NSDate date]] capitalizedString];
    
    formatter.dateFormat=@"EEEE";
    NSString *dayString = [[formatter stringFromDate:[NSDate date]] capitalizedString];
    
    _lbe_Weekdays.text=[NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,dateString];
    
}




#pragma mark
#pragma mark UIButton Custom Methods
#pragma mark

- (IBAction)setting_btn:(id)sender {
    
}

- (IBAction)imgSave_btn:(id)sender {
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //[self.viewInfoThemeSave setHidden:NO];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    saveClicked = YES;
    
    NSString *strVal = delegate.imagesArray[pageNo] ;
    
    if ([strVal containsString:@"mp4"]) {
        if ([_strKey isEqualToString:@"CrimsonCarrot"]) {
             _infoPointsUsed.text = @"500  points needed to save";
        }
        else{
        _infoPointsUsed.text = @"300  points needed to save" ;
            
        }
    
    }else{
        
          _infoPointsUsed.text = @"100  points needed to save" ;
        
    }
    
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == false)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"keychain"] == false){
            self.viewFirst.hidden = YES;
            self.viewSecond.hidden = YES;
            self.viewThird.hidden = NO;
            self.viewFourth.hidden = YES;
            self.viewFive.hidden = YES;
            self.viewSix.hidden = YES;
            self.viewSeven.hidden = YES;
            
            
            
            self.infoCloseThemeee.enabled = NO;
            self.btnMenu.enabled = NO;
            self.btnMenu.enabled = NO;
            _themeCloseBtnn.enabled = NO;
            _btnCoins.enabled = NO;
            _btnLandscapeCoins.enabled = NO;
            
            
            [_infoCloseThemeee setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            [self.view bringSubviewToFront:_viewThird];
            [self.view bringSubviewToFront:_downArrowImg];
            
            
            
            
        }else{
            
            [_infoCloseThemeee setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
    }else{
        [_infoCloseThemeee setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
    }
    
    
    
    infoClicked = true ;
    [self.btnClose setHidden:YES];
    [self.bottomView setHidden:YES];
    
    
    [self.btnClose setHidden:YES];
    [self.btnTheme setHidden:YES];
    [_imgCoin setHidden:NO];
    [_txtCoin setHidden:NO];
    
    NSInteger center = [UIScreen mainScreen].bounds.size.height/2;
    self.info_view.hidden = false;
    [self.info_Btn setSelected:YES];
    
    [_saveBtn setHidden:NO];
    
    
    [self.txtView setContentOffset:CGPointZero animated:YES];
    
    
    
//    [self.view bringSubviewToFront:_scrllView];
    // _bottomView.backgroundColor = [UIColor colorWithRed:31/255.0f green:33/255.0f blue:36/255.0f alpha:1];
    
    
    
    [self.view bringSubviewToFront:_btnClose];
    
    
    self.viewMovableHeight.constant = 125 +_txtView.frame.size.height +140 ;
    
    [self.view layoutIfNeeded];
    

     if (delegate.arrayKey.count >0) {
    if (pageNo != (int)nil) {
        strId = [NSString stringWithFormat:@"%@",[[delegate.arrayKey  valueForKey:@"id"] objectAtIndex:pageNo]];
        NSLog(@"%@",strId);
        
    }else{
        
        strId = [NSString stringWithFormat:@"%@",[[delegate.arrayKey  valueForKey:@"id"] objectAtIndex:0]];
    }
    
     }
    
   [self getImagesFromServer];
    
    [self.bottomView setHidden:YES];
   
    self.imagesScrollView.scrollEnabled = NO;
    _btnMenu.userInteractionEnabled = NO;
    
    
    [self.bottomView setHidden: YES];
    
    
   _slider.hidden = YES;
    
    _landscapeSlider.hidden = YES;
    
    [_btnTheme setHidden:YES];
    
}

-(void)SaveImgage{
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self.view bringSubviewToFront:_ViewSaved];
    
    totalcoins = [[NSString stringWithFormat:@"%@", _btnCoins.titleLabel.text]intValue];
    totalcoins = [[NSString stringWithFormat:@"%@", _btnLandscapeCoins.titleLabel.text]intValue];
    NSLog(@"%d",totalcoins);
//    totalcoins = 1000;
    [keys addObject:delegate.arrayKey[pageNo]];
    
    NSString *strVal = delegate.imagesArray[pageNo] ;
    
    if ([strVal containsString:@"mp4"]) {
        
        if ([_strKey isEqualToString:@"CrimsonCarrot"]){
            
            {
                if (totalcoins < 500){
                    
                    [self.ViewSaved setHidden:YES];
                    
                    self.TxtThemeAdded.text = @"You do not have enough points to save this image.";
                    self.TxtThemeAdded.textColor = [UIColor colorWithRed:212/255.0f green:58/255.0f blue:58/255.0f alpha:1];
                    
                    
                    _txtPoints.text = @"             Points needed";
                    _txtUsedCoins.text = @"500";
                    
                }else{
                    
                    _txtPoints.text = @"             Points used";
                    if (delegate.arrayKey.count >0 )
                    {
                        [self savingThemesBycategory];
                        
                    }
                }
            }
            
        }else{
            
            if (totalcoins < 300){
                
                [self.ViewSaved setHidden:YES];
                
                self.TxtThemeAdded.text = @"You do not have enough points to save this image.";
                self.TxtThemeAdded.textColor = [UIColor colorWithRed:212/255.0f green:58/255.0f blue:58/255.0f alpha:1];
                
                _txtPoints.text = @"             Points needed";
                _txtUsedCoins.text = @"300";
                
                
            }else{
                
                
                _txtPoints.text = @"             Points used";
                if (delegate.arrayKey.count >0 )
                {
                    [self savingThemesBycategory];
                    
                }
                
                
            }

            
            
        }
      
        
    }else{
        
        if (totalcoins < 100){
            
            
            
            [self.ViewSaved setHidden:YES];
            
            self.TxtThemeAdded.text = @"You do not have enough points to save this image.";
            self.TxtThemeAdded.textColor = [UIColor colorWithRed:212/255.0f green:58/255.0f blue:58/255.0f alpha:1];
            
            
            _txtPoints.text = @"             Points needed";
            _txtUsedCoins.text = @"100";
            
            
        }else{
            
            _txtPoints.text = @"             Points used";
            if (delegate.arrayKey.count >0 )
            {
                [self savingThemesBycategory];
                
            }
       }

   }
    
    
        SavedCollectionCount = (int)imgArray.count ;
        NSLog(@"%d",SavedCollectionCount);
        
        
        NSUserDefaults *userdefault17 = [NSUserDefaults standardUserDefaults];
        [userdefault17 setInteger:SavedCollectionCount forKey:@"SavedCollectionCount"];
        [userdefault17 synchronize];
        
         [SSKeychain setPassword:[NSString stringWithFormat:@"%d",SavedCollectionCount]   forService:@"SavedCollectionCount" account:@"SavedCollectionCount"];
        
        
        totalcoins = [[NSString stringWithFormat:@"%@", _btnCoins.titleLabel.text]intValue];
        
//         NSString *str1  = [delegate.imagesArray objectAtIndex:pageNo];
//        
//        NSLog(@"%d",totalcoins);
    
        
        
        
        if ([strVal containsString:@"mp4"]) {
            
            if (  [_strKey  isEqual: @"CrimsonCarrot"]){
                if (totalcoins>=500) {
                    numberOfCoins =  totalcoins - 500 ;
                    NSLog(@"%d",numberOfCoins);
                    _txtUsedCoins.text = [NSString stringWithFormat:@"%d",500];
                    self.TxtThemeAdded.text =@"This theme has been added to your collection!";
                    self.TxtThemeAdded.textColor = [UIColor colorWithRed:219/255.0f green:162/255.0f blue:23/255.0f alpha:1];
                    
                }else{
                    
                    numberOfCoins = [[NSString stringWithFormat:@"%@", _btnCoins.titleLabel.text]intValue];
                }
                
            }else{
                
                if (totalcoins>=300) {
                    numberOfCoins =  totalcoins - 300 ;
                    NSLog(@"%d",numberOfCoins);
                    _txtUsedCoins.text = [NSString stringWithFormat:@"%d",300];
                    self.TxtThemeAdded.text =@"This theme has been added to your collection!";
                    self.TxtThemeAdded.textColor = [UIColor colorWithRed:219/255.0f green:162/255.0f blue:23/255.0f alpha:1];
                    
                }else{
                    
                    numberOfCoins = [[NSString stringWithFormat:@"%@", _btnCoins.titleLabel.text]intValue];
                }
            }
            
            
            

        }else{
            
             if (totalcoins>=100) {
            numberOfCoins =  totalcoins - 100 ;
            NSLog(@"%d",numberOfCoins);
            
            _txtUsedCoins.text = [NSString stringWithFormat:@"%d",100];
                 
                 self.TxtThemeAdded.text =@"This theme has been added to your collection!";
                         self.TxtThemeAdded.textColor = [UIColor colorWithRed:219/255.0f green:162/255.0f blue:23/255.0f alpha:1];

             }
            else{
                
                numberOfCoins = [[NSString stringWithFormat:@"%@", _btnCoins.titleLabel.text]intValue];
                
                
            }

        }
    
        
        ///blink hamburgerMenuRedIcon
        
        _viewRed.hidden = NO;
        _viewRed.alpha = 1.0f;
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:1.0 delay:1.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            _viewRed.alpha = 0.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            _viewRed.alpha = 1.0f;
            [self.view bringSubviewToFront:_viewRed];
        }];
        
        NSString*str = [NSString stringWithFormat:@"%@",self.infoCollectionName.text];
        
        [collectionName addObject: str];
        
        
        NSUserDefaults *userdefault18 = [NSUserDefaults standardUserDefaults];
        [userdefault18 setObject:collectionName forKey:@"collectionName"];
        [userdefault18 synchronize];
        
          [SSKeychain setPassword:[NSString stringWithFormat:@"%@",collectionName]   forService:@"collectionName" account:@"collectionName"];
    
    
    _txtBalance.text = [NSString stringWithFormat:@"%d",numberOfCoins];
    
    [[NSUserDefaults standardUserDefaults]setInteger:numberOfCoins  forKey:@"coins"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]setObject:self.lbe_Time.text forKey:@"time"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [SSKeychain setPassword:_txtBalance.text forService:@"image" account:@"imagelordData"];
    
    
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"keychain"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSInteger screenWidth = [UIScreen mainScreen].bounds.size.width;
    [self.scrllView setContentOffset:CGPointMake(screenWidth, 0)];
    [self.ViewSaved setHidden:NO];
    [self viewWillAppear:YES];
    [self postThemePurchase];
    
}

- (IBAction)folder_btn:(id)sender {
    
    [self playAnimatedTheme];
    
}

-(void)playAnimatedTheme{
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    avPlayer = (AVPlayer*)[avPlayerArray objectAtIndex:pageNo];
    NSLog(@"%@",avPlayerArray);
    
    
    
    _btnTheme.tintColor = [UIColor clearColor];
    
    
    if (!_btnTheme.isSelected) {
        [_btnTheme setSelected:YES];
        
        [_btnTheme.layer removeAllAnimations];
        
        [self.btnTheme setBackgroundImage:[UIImage imageNamed:@"livephoto-pause-icon.png"] forState:UIControlStateNormal];
        
        
        
        [avPlayer play];
        
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        
        currentItem = avPlayer.currentItem;
        
        
        NSTimeInterval totalTime = CMTimeGetSeconds(currentItem.duration);
        
        _slider.maximumValue = totalTime;
        _landscapeSlider.maximumValue = totalTime;
        
        
    }else{
        
        [_btnTheme setSelected:NO];
        [avPlayer pause];
        
        [self.btnTheme setBackgroundImage:[UIImage imageNamed:@"livephoto-icon.png"] forState:UIControlStateNormal];
        
    }
    
    
    if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
    {
        
        _sliderTop.constant  = 2 ;
        
        
        
    }else{
        
        //_sliderTop.constant  = -15 ;
        
    }
    
    NSString*str1 = delegate.imagesArray[pageNo];
    
    
    if ([str1 containsString:@"mp4"]) {
        [_btnTheme setHidden:YES];
        self.slider.hidden = NO;
        self.landscapeSlider.hidden = YES ;
        
        
    }else{
        [_btnTheme setHidden:YES];
        
        self.slider.hidden = YES;
        self.landscapeSlider.hidden = YES ;
    }
    
    
    if(isPortrait== YES){
        
        self.slider.hidden = NO;
        self.landscapeSlider.hidden = YES;
        
    }else{
        self.slider.hidden = YES;
        self.landscapeSlider.hidden = NO;
        
    }
}

- (void)updateSlider {
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    NSLog(@" Capturing Time :%f ",currentTime);
    _slider.value =currentTime;
    _landscapeSlider.value =currentTime;
    
}


- (IBAction)homeClicked:(id)sender {
    
    [self.btnClose setHidden:YES];
    //[self.btnTheme setHidden:NO];
    [self.btnTheme setHidden:YES];
    [_imgCoin setHidden:NO];
    [_txtCoin setHidden:NO];
    
    
    
    
    
    int  y = 0 ;
    [self.view bringSubviewToFront:_btnMenu];
    //[self.view bringSubviewToFront:_bottomView];
    [self.info_Btn setSelected:false];
    
    
    [UIView animateWithDuration:0.6
                          delay:0.2
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         self.scrllView.hidden= NO;
         
         self.scrllView.contentOffset = CGPointMake(0, y);
         _imgGradient.alpha = 0;
         [_info_Btn setAlpha:1.0];
         
         [self.view layoutIfNeeded];
         
         self.scrllView.hidden= NO;
         _saveBtnBottom.constant = 40 ;
         self.imgSwipe.alpha =1.0 ;
         self.viewinfoHight.constant =0.0f;
         self.saveBtnBottomLayOut.constant = 17.0f;
         self.setting_btnBottom.constant = 78.0f;
         self.viewLikeLayOut.constant = -202.0f;
         self.btnMenu.alpha = 1.0;
         self.btnMenu.alpha = 1.0;
         
         _saveBtnBottom.constant = 194 ;
         _heightCoinsLayout.constant = 205;
         
         [self.view layoutIfNeeded];
         _bottomView.backgroundColor = [UIColor clearColor];
         
         
         
     }
                     completion:^(BOOL finished)
     {
         [self.imgSwipe setHidden:YES];
         
         if(finished)
         {
             
             [self.saveBtn setHidden:NO];
         }
         
     }];
    
}

- (IBAction)menuClicked:(id)sender {
    
    //  [self.menuTableview setContentOffset:CGPointMake(0,) animated:YES];
    
   
    
    
    [self.view bringSubviewToFront:_btnMenu];
    
    [_btnMenu setSelected:YES];
    [self menuShow];
    [self.info_Btn setHidden:YES];
    [_btnMenu setHidden:YES];
    
    
    _txtCoinsCount.text = self.btnCoins.titleLabel.text ;
    
    // self.imagesScrollView.scrollEnabled = NO;
    [self.view bringSubviewToFront:_slidingView];
    
    _viewRed.alpha =0.0;
    
    [self.view bringSubviewToFront:_slidingView];
    
    [self.menuTableview reloadData];
    
    
}

- (IBAction)closeClicked:(id)sender{
    
    infoClicked = false ;
    [self.btnClose setHidden:YES];
    // [self.btnTheme setHidden:NO];
   
    [_imgCoin setHidden:NO];
    [_txtCoin setHidden:NO];
    
    int  y = 0 ;
    [self.view bringSubviewToFront:_btnMenu];
    //[self.view bringSubviewToFront:_bottomView];
    [self.info_Btn setSelected:false];
    // [_btnTheme setHidden:NO];
    [self playBtn];
    
    
    
    [UIView animateWithDuration:0.6
                          delay:0.2
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         self.scrllView.hidden= NO;
         
         
         [self.info_Btn setAlpha:1.0];
         
         
         self.scrllView.contentOffset = CGPointMake(0, y);
         _imgGradient.alpha = 0;
         
         [self.view layoutIfNeeded];
         
         self.scrllView.hidden= NO;
         _saveBtnBottom.constant = 40 ;
         
         
         self.saveBtn.alpha = 1.0;
         self.txtCoinsSave.alpha = 1.0;
         
         self.lbe_Time.alpha = 1.0;
         self.lbe_Weekdays.alpha = 1.0;
         
         
         self.imgSwipe.alpha =1.0 ;
         self.viewinfoHight.constant =0.0f;
         
         self.setting_btnBottom.constant = 78.0f;
         self.viewLikeLayOut.constant = -202.0f;
         self.btnMenu.alpha = 1.0;
         
         [self.view layoutIfNeeded];
         _bottomView.backgroundColor = [UIColor clearColor];
         
         
         
     }
                     completion:^(BOOL finished)
     {
         [self.imgSwipe setHidden:YES];
         
         
         if(finished)
         {
             
             [self.saveBtn setHidden:NO];
         }
         
     }];
    
    
    [self.view sendSubviewToBack:self.scrllView];
    
    
    self.imagesScrollView.scrollEnabled = YES;
    
    
}

- (IBAction)closeTableClicked:(id)sender {
    
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    _viewRed.hidden = YES;
    _viewRed.alpha =1.0;
    [self.view bringSubviewToFront:_viewRed];
    
    
    [self.info_Btn setHidden:YES];
    self.imagesScrollView.scrollEnabled = YES;
    [_btnClose setHidden:YES];
    [_btnMenu setHidden:NO];
    if (delegate.arrayKey.count == 0) {
        _info_Btn.hidden = YES;
        _btnTheme.hidden = YES;
        _lbe_Time.hidden = YES;
        _lbe_Weekdays.hidden = YES;
        
        _txtSwipe.hidden = YES;
        
        
        if (deepLink == YES) {
            self.viewNoThemes.hidden = YES;
        }else{
            self.viewNoThemes.hidden = NO;
            
        }
        
        
        //  [self.btnTheme setHidden:NO];
        [self.btnTheme setHidden:YES];
        [self.bottomView setHidden:YES];
        
        [self.view bringSubviewToFront:self.btnTheme];
        //[self.view bringSubviewToFront:self.bottomView];
        
    }else{
        _info_Btn.hidden = YES;
        //_btnTheme.hidden = NO;
        _btnTheme.hidden = YES;
        _lbe_Time.hidden = NO;
        _lbe_Weekdays.hidden = NO;
        _bottomView.hidden = YES;
        _txtSwipe.hidden = NO;
        self.viewNoThemes.hidden = YES;
        
        
        
    }
    
    [self menuHide];

    
    //     if ([videoStr containsString:@"mp4"]) {
    //         [_btnTheme setHidden:NO];
    //     }else{
    //
    //          [_btnTheme setHidden:YES];
    //     }
    
}
- (IBAction)btnAddpoints:(id)sender {
    
    PointsViewController *pVc = [[PointsViewController alloc]init];
    
    pVc = [self.storyboard instantiateViewControllerWithIdentifier:@"PointsViewController"];
    pVc.strCoins = self.btnCoins.titleLabel.text ;
    pVc.strCoins = self.btnLandscapeCoins.titleLabel.text ;
    [self.navigationController pushViewController:pVc animated:YES];
    
}

- (IBAction)btnCoinsClicked:(id)sender {
    
    PointsViewController *pVc = [[PointsViewController alloc]init];
    
    pVc = [self.storyboard instantiateViewControllerWithIdentifier:@"PointsViewController"];
    pVc.strCoins = self.btnCoins.titleLabel.text ;
    pVc.strCoins = self.btnLandscapeCoins.titleLabel.text ;
    
    [self.navigationController pushViewController:pVc animated:YES];
    
    
}
- (IBAction)themeClicked:(id)sender {
    MyThemesController *my = [[ MyThemesController alloc]init];
    my = [self.storyboard instantiateViewControllerWithIdentifier:@"mythemes"];
    [self.navigationController pushViewController:my animated:YES];
    
    
}
- (IBAction)closeViewClicked:(id)sender {
    [self.ViewSaved setHidden:YES];
    [self sendScrollDescriptiontoBack];
    infoClicked = false ;
    [self.bottomView setHidden:YES];
    [self.view bringSubviewToFront:_viewRed];
    self.imagesScrollView.scrollEnabled = YES;
    _btnMenu.userInteractionEnabled = YES;
    
    [self playBtn];
    
    
}
- (IBAction)getCoinsClicked:(id)sender {
    
}
- (IBAction)morePointsClicked:(id)sender {
    PointsViewController *pVc = [[PointsViewController alloc]init];
    
    pVc = [self.storyboard instantiateViewControllerWithIdentifier:@"PointsViewController"];
    
    pVc.strCoins = self.btnCoins.titleLabel.text ;
    pVc.strCoins = self.btnLandscapeCoins.titleLabel.text ;
    
    [self.navigationController pushViewController:pVc animated:YES];
}
- (IBAction)doneClickeddd:(id)sender {
    [self.ViewSaved setHidden:YES];
    [self sendScrollDescriptiontoBack];
    infoClicked = false ;
    [self.btnClose setHidden:YES];
    [self.bottomView setHidden:YES];
    //  [self.btnTheme setHidden:NO];
    [self.btnTheme setHidden:YES];
    [self.view bringSubviewToFront:_viewRed];
    
    //  [self SHARE];
    
    self.imagesScrollView.scrollEnabled = YES;
    _btnMenu.userInteractionEnabled = YES;
    
    [self playBtn];
    
}


- (IBAction)infoSaveThemeClicked:(id)sender {
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self imgSave_btn:nil];
    [_ViewSaved setHidden:NO];
    [_viewInfoThemeSave setHidden:YES];
    
    if (infoClicked== true) {
        _themeNamee.text =   _infoThemeName.text ;
        
    }
    
    [self.view bringSubviewToFront:_ViewSaved];
    
    if (delegate.arrayKey.count == 0) {
        _imagesScrollView.hidden = YES;
        [_saveBtn setHidden:YES];
        
        _info_Btn.hidden = YES;
        _scrllView.scrollEnabled = NO;
        _btnTheme.hidden = YES;
        _lbe_Time.hidden = YES;
        _lbe_Weekdays.hidden = YES;
         _bottomView.hidden = YES;
        _txtSwipe.hidden = YES;
       
        
        self.viewNoThemes.hidden = NO;
        
        
    }
    
    [self.bottomView setHidden:YES];
    [self.btnTheme setHidden:YES];
    
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = NO;
    self.viewFive.hidden = YES;
    self.viewSix.hidden = YES;
    self.viewSeven.hidden = YES;
    self.lblFourth.hidden = YES;
    
    
    
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == false)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"keychain"] == false){
            _txtNewBalance.textColor = [UIColor grayColor];
            _txtBalance.textColor = [UIColor grayColor];
            _txtPoints.textColor = [UIColor grayColor];
            _btnGetMore.enabled = NO;
            _doneClickedd.enabled = NO;
            self.lblFourth.hidden = NO;
            
            [_btnGetMore setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            [_doneClickedd setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
            _btnCloseee.enabled = NO;
            
            _viewCoinsss.layer.borderWidth = 0.0 ;
            
            [self.view sendSubviewToBack:_btnCloseee];
            
            
            // [self.view bringSubviewToFront:self.viewFirstTime];
            
            
            [self.view bringSubviewToFront:self.ViewSaved];
            [self.view bringSubviewToFront:self.viewFourth];
            
            [self.view bringSubviewToFront:self.btnNextFourth];
            [self.btnNextFourth setEnabled:YES];
            [self.viewFourth setHidden:NO];
            
        }else{
            
            [self.viewFourth setHidden:YES];
            
        }
        
    }
    else{
        
        [self.viewFourth setHidden:YES];
        
    }
    
    [self SaveImgage];
    
    
    
    self.imagesScrollView.scrollEnabled = NO;
    self.btnMenu.userInteractionEnabled = NO;
    
}


- (IBAction)infoCloseClicked:(id)sender {
    
    self.viewInfoThemeSave.hidden = YES;
    [self sendScrollDescriptiontoBack];
    infoClicked = false ;
    
    saveClicked = NO;
    
    // [self SHARE];
    
    self.imagesScrollView.scrollEnabled = YES;
    _btnMenu.userInteractionEnabled = YES;
    
    [self playBtn];
    
    
}

- (IBAction)ViewinfoCloseClick:(id)sender {
    [self sendScrollDescriptiontoBack];
    [_ViewSaved setHidden:YES];
    self.viewInfoThemeSave.hidden = YES;
    
    infoClicked = false ;
    
    [self.btnClose setHidden:YES];
    [self.bottomView setHidden:YES];
    saveClicked = NO;
    
    self.imagesScrollView.scrollEnabled = YES;
    
    _btnMenu.userInteractionEnabled = YES;
    
    [self playBtn];
}

-(void)noThemesMethod{
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    delegate.imagesArray = [[NSMutableArray alloc]init];
    self.saveBtn.hidden = YES ;
    [self closeTableClicked:nil];
    self.info_Btn.hidden = YES;
    imageView = nil;
    _imagesScrollView.hidden = YES;
    _scrllView.hidden = YES;
    _scrllView.scrollEnabled = NO;
    _txtCoinsSave.hidden= YES;
    _btnTheme.hidden = YES;
    _lbe_Time.hidden = YES;
    _lbe_Weekdays.hidden = YES;
    
    _txtSwipe.hidden = YES;
    _txtTap.hidden = YES;
    
    self.viewNoThemes.hidden = NO;
    [self.view bringSubviewToFront:_btnCoins];
    [self.view bringSubviewToFront:_btnLandscapeCoins];
    [self.view bringSubviewToFront:_btnTheme];
   // [self.view bringSubviewToFront:_bottomView];
    
    strId = @"" ;
    
    
    
}

-(void)getDescription {
    
    
//    description = [[[NSMutableArray alloc]initWithArray:[self.arrayKey  valueForKey:@"description"]]mutableCopy];
//    
//    
//    
//    
//    name = [[[NSMutableArray alloc]initWithArray:[self.arrayKey  valueForKey:@"name"]]mutableCopy];
//    collection = [[[NSMutableArray alloc]initWithArray:[self.arrayKey  valueForKey:@"collection"]]mutableCopy];
//    idd = [[[NSMutableArray alloc]initWithArray:[self.arrayKey  valueForKey:@"id"]]mutableCopy];
//    
//    
//    
//    
//    if (idd.count >0){
//        
//        strId = [NSString stringWithFormat:@"%@",[idd objectAtIndex:0]];
//        
//    }
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (delegate.arrayKey.count >0) {
        
        
        if (pageNo != (int)nil) {
            strId = [NSString stringWithFormat:@"%@",[[delegate.arrayKey  valueForKey:@"id"] objectAtIndex:pageNo]];
            NSLog(@"%@",strId);
            
        }else{
            
            strId = [NSString stringWithFormat:@"%@",[[delegate.arrayKey  valueForKey:@"id"] objectAtIndex:0]];
    }
    
}
    
   
    
    
}


#pragma mark
#pragma mark UITableView DataSource Methods
#pragma mark

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [menuItemsArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    return menuItemsArray.count;
    
    
        if (section == menuItemsArray.count-1)
        {
                return collectionsArray.count ;
            
        }
            else{
                
                return 0 ;
            }
            
            
      
    
}

    


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
//    cell.backgroundColor = [UIColor colorWithRed:255/255.0F green:255/255.0F blue:255/255.0F alpha:0.2];
    
    [_menuTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if (indexPath.section == menuItemsArray.count -1) {
        
            cell.textLabel.text = [NSString stringWithFormat:@"  %@",[collectionsArray objectAtIndex:indexPath.row]];
        
        }
        
        
        
    else{
     ///   cell.textLabel.text = @"" ;
        
        
    }
    
    
    UIImageView *imgg = [[UIImageView alloc]initWithFrame:CGRectMake(0,280, self.view.frame.size.width, self.menuTableview.frame.size.height)];
    
    imgg.image  =  [UIImage imageNamed:@"Rectangle 2"];
   
    imgg.alpha = 0.5 ;
    
   // cell.textLabel.text = [menuItemsArray objectAtIndex:indexPath.section];
    
    if (indexPath.section == 0) {
        cell.textLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
    }
    else{
    cell.textLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        
    }
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    
    
    
    
    
     if (  [_strKey  isEqual: @"Creature"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Creature"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    
    else if ([_strKey  isEqual: @"Character"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Character"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
        }
        
    }
    
    else if (  [_strKey  isEqual: @"Environment"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Environment"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    } else if (  [_strKey  isEqual: @"DigitalArt"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Digital Art"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    else if (  [_strKey  isEqual: @"Illustration"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Illustration"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    else if (  [_strKey  isEqual: @"Fantasy"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Fantasy"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    else if (  [_strKey  isEqual: @"ConceptArt"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Concept Art"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    
    
    else if (  [_strKey  isEqual: @"Game Art"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Game Art"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    else if (  [_strKey  isEqual: @"Horror"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Horror"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    
    else if (  [_strKey  isEqual: @"Comic Art"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Comic Art"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    else if (  [_strKey  isEqual: @"IndustrialDesign"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Industrial Design"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    } else if (  [_strKey  isEqual: @"Landscape"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Landscape"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }else if (  [_strKey  isEqual: @"Portrait"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Portrait"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }
    
    else if (  [_strKey  isEqual: @"Vehicle"])
    {
        if ([collectionsArray[indexPath.row] isEqualToString:@"Vehicle"]) {
            cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
        }else{
            
            cell.textLabel.textColor = [UIColor whiteColor];
            
            
        }
        
    }



    
    
 

    
    
   
    
//    
//    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Rectangle 2"]];
//    
//    cell.textLabel.textColor
//    = [UIColor blackColor];
//
// _menuTableview.separatorColor = [UIColor clearColor];
    
  
    
    cell.backgroundColor = [UIColor colorWithRed:64/255.0f green:64/255.0f blue:64/255.0f alpha:0.9];
    
    
    
    
    return cell;
    
}

#pragma mark
#pragma mark UITableView Delegate Methods
#pragma mark

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    //from api
    
    //    NSMutableArray *arr =  [arrayAdd objectAtIndex:indexPath.row];
    //    NSLog(@"%@",arr);
    //
    //    description = [[arr objectAtIndex:indexPath.row]valueForKey:@"description"];
    //    NSLog(@"%@",description);
    //    idd = [[arr objectAtIndex:indexPath.row]valueForKey:@"id"];
    //    NSLog(@"%@",description);
    //    name = [[arr objectAtIndex:indexPath.row]valueForKey:@"name"];
    //    NSLog(@"%@",description);
    //    collection = [[arr objectAtIndex:indexPath.row]valueForKey:@"category"];
    //     NSLog(@"%@",collection);
    //
    //    delegate.imagesArray = [[arr objectAtIndex:indexPath.row]valueForKey:@"url"];
    //
    //
    
    
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    UITableViewCell * cell = [self.menuTableview cellForRowAtIndexPath:indexPath];
    
   
    
    for (id object in cell.superview.subviews) {
        if ([object isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *cellNotSelected = (UITableViewCell*)object;
            cellNotSelected.textLabel.textColor = [UIColor whiteColor];
        }
    }
    
    cell.textLabel.textColor = [UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
   // cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cellColor.png"]];

    _menuTableview.separatorColor = [UIColor clearColor];
    
    
    
    _imagesScrollView.contentOffset = CGPointZero;
    
    
    self.saveBtn.alpha =1.0 ;
    _txtCoinsSave.alpha = 1.0;
    self.viewNoThemes.hidden = YES;
    
    _imagesScrollView.hidden = NO;
    self.saveBtn.hidden = NO ;
    self.info_Btn.hidden = YES;
    _scrllView.scrollEnabled = YES;
    //_btnTheme.hidden = NO;
    _btnTheme.hidden = YES;
    
    _lbe_Time.hidden = NO;
    _lbe_Weekdays.hidden = NO;
    _bottomView.hidden = YES;
    _txtSwipe.hidden = NO;
    
    
    
    

    
    
   //if (indexPath.row == 0) {
//        MyThemesController *my = [[ MyThemesController alloc]init];
//        my = [self.storyboard instantiateViewControllerWithIdentifier:@"mythemes"];
//        my.arrayImages = [NSMutableArray arrayWithArray:delegate.myCollectionArray];
//        [self.navigationController pushViewController:my animated:YES];
        
  //  }
    
    
//    else if ([menuItemsArray[indexPath.row] isEqualToString:@"Featured"]) {
//        //Featured
//        delegate.imagesArray = [[NSMutableArray alloc]init];
//
//        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.featuredkeys];
//        self.strKey = @"Featured";
//        if (delegate.arrayKey.count >0) {
//            
//            
//            delegate.isNoRecord = false ;
//        
//                NSLog(@"%@",delegate.imagesArray);
//            for (NSMutableDictionary * data in delegate.featuredkeys) {
//                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
//                if ([strImage containsString:@"mp4"]) {
//                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
//                                                                   withString:@""];
//                    
//                }
//                
//                [delegate.imagesArray addObject:strImage];
//            }
//            
//            [self getDescription];
//            [self scrollimages];
//            [self closeTableClicked:nil];
//            [self getImagesFromServer];
//            
//            _slider.hidden = YES;
//             _landscapeSlider.hidden = YES;
//            
//            
//        }
//        else{
//             delegate.isNoRecord = true;
//            [self noThemesMethod];
//            
//        }
//        
//    }
//    else if ([menuItemsArray[indexPath.row] isEqualToString:@"The Crimson Carrot"]) {
//        //Featured
//        delegate.imagesArray = [[NSMutableArray alloc]init];
//        
//        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.crimpsonCarrotKeys];
//        self.strKey = @"CrimsonCarrot";
//        if (delegate.arrayKey.count >0) {
//            
//            
//            delegate.isNoRecord = false ;
//            
//            NSLog(@"%@",delegate.imagesArray);
//            for (NSMutableDictionary * data in delegate.crimpsonCarrotKeys) {
//                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
//                if ([strImage containsString:@"mp4"]) {
//                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
//                                                                   withString:@""];
//                    
//                }
//                
//                [delegate.imagesArray addObject:strImage];
//            }
//            
//            NSLog(@"%@",delegate.imagesArray);
//            
//            
//            [self getDescription];
//            [self scrollimages];
//            [self closeTableClicked:nil];
//            [self getImagesFromServer];
//            
//            _slider.hidden = YES;
//            _landscapeSlider.hidden = YES;
//            
//            
//        }
//        else{
//            delegate.isNoRecord = true;
//            [self noThemesMethod];
//            
//        }
//        
//    }
//    
//    
//    else if ([menuItemsArray[indexPath.row] isEqualToString:@"Live Screens"]) {
//        //Featured
//        delegate.imagesArray = [[NSMutableArray alloc]init];
//        
//        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.liveKeys];
//        self.strKey = @"Live";
//        if (delegate.arrayKey.count >0) {
//            
//            
//            delegate.isNoRecord = false ;
//            
//            
//            NSLog(@"%@",delegate.imagesArray);
//            
//            
//            for (NSMutableDictionary * data in delegate.liveKeys) {
//                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
//                if ([strImage containsString:@"mp4"]) {
//                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
//                                                                   withString:@""];
//                    
//                }
//                
//                [delegate.imagesArray addObject:strImage];
//            }
//            
//            [self getDescription];
//            [self scrollimages];
//            [self closeTableClicked:nil];
//            [self getImagesFromServer];
//            
//            _slider.hidden = YES;
//            _landscapeSlider.hidden = YES;
//            
//            
//        }
//        else{
//            delegate.isNoRecord = true;
//            [self noThemesMethod];
//            
//        }
//        
//    }
//
//    
//    else
    
    
    if ([collectionsArray[indexPath.row] isEqualToString:@"Fantasy"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

       delegate.Fantasykeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Fantasy"]) {
                [delegate.Fantasykeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Fantasykeys];
        self.strKey = @"Fantasy";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Fantasykeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }

            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }else if ([collectionsArray[indexPath.row] isEqualToString:@"Illustration"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Illustrationkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Illustration"]) {
                [delegate.Illustrationkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Illustrationkeys];
        self.strKey = @"Illustration";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Illustrationkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Character"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.characterkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Character"]) {
                [delegate.characterkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.characterkeys];
        self.strKey = @"Character";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.characterkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    
    
    
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Digital Art"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.DigitalArtkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Digital Art"]) {
                [delegate.DigitalArtkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.DigitalArtkeys];
        self.strKey = @"DigitalArt";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.DigitalArtkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Portrait"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Portraitkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Portrait"]) {
                [delegate.Portraitkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Portraitkeys];
        self.strKey = @"Portrait";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Portraitkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Concept Art"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.conceptArtkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Concept Art"]) {
                [delegate.conceptArtkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.conceptArtkeys];
        self.strKey = @"Concept Art";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.conceptArtkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Environment"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Environmentkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Environment"]) {
                [delegate.Environmentkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Environmentkeys];
        self.strKey = @"Environment";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Environmentkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }else if ([collectionsArray[indexPath.row] isEqualToString:@"Vehicle"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.vehiclekeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Vehicle"]) {
                [delegate.vehiclekeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.vehiclekeys];
        self.strKey = @"Vehicle";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.vehiclekeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Game Art"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.gameArtkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Game Art"]) {
                [delegate.gameArtkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.gameArtkeys];
        self.strKey = @"Game Art";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.gameArtkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Comic Art"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.ComicArtkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Comic Art"]) {
                [delegate.ComicArtkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.ComicArtkeys];
        self.strKey = @"Comic Art";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.ComicArtkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Industrial Design"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.IndustrialDesignkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Industrial Design"]) {
                [delegate.IndustrialDesignkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.IndustrialDesignkeys];
        self.strKey = @"IndustrialDesign";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.IndustrialDesignkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    
    
    
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Landscape"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Landscapekeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Landscape"]) {
                [delegate.Landscapekeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Landscapekeys];
        self.strKey = @"Landscape";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Landscapekeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }else if ([collectionsArray[indexPath.row] isEqualToString:@"Creature"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Creaturekeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Creature"]) {
                [delegate.Creaturekeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Creaturekeys];
        self.strKey = @"Creature";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Creaturekeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    else if ([collectionsArray[indexPath.row] isEqualToString:@"Horror"]) {
        //fantasy
        delegate.imagesArray = [[NSMutableArray alloc]init];

        delegate.Horrorkeys = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in delegate.allkeys) {
            if ([dict[@"collection"]isEqualToString:@"Horror"]) {
                [delegate.Horrorkeys addObject:dict];
            }
        }
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.Horrorkeys];
        self.strKey = @"Horror";
        
        if (delegate.arrayKey.count >0) {
            delegate.isNoRecord = false ;
            for (NSMutableDictionary * data in delegate.Horrorkeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            
            
            [self getDescription];
            [self.scrllView setNeedsDisplay];
            [self.view setNeedsDisplay];
            [imageView setNeedsDisplay];
            
            [self scrollimages];
            
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
        } else{
            [self noThemesMethod];
            delegate.isNoRecord = true;
            
        }
        
    }
    
    
}
        







- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == menuItemsArray.count - 1) {
        if (closeSection) {
            return 0;
        }else{
            return  70;
        }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (tableView == _menuTableview ) {
        return 70;
    }
    return 0 ;
}
-(void)menuShow {
    
    
//    if ([UIScreen mainScreen].bounds.size.width == 375) {
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = 0 ;
//            _viewWidth.constant = 295 ;
//            x = self.viewWidth.constant -42 ;
//            [UIView animateWithDuration:0.1 animations:^{
//                _menuBtnLeading.constant = x ;
//                [self.view layoutIfNeeded];
//                
//            }];
//            
//            [self.view layoutIfNeeded];
//        }];
//        
//    }
//    else if ([UIScreen mainScreen].bounds.size.width == 414) {
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = 0;
//            _viewWidth.constant = 362;
//            x = self.viewWidth.constant -42 ;
//            [UIView animateWithDuration:0.1 animations:^{
//                _menuBtnLeading.constant = x ;
//                [self.view layoutIfNeeded];
//                
//            }];
//            [self.view layoutIfNeeded];
//        }];
//        
//        
//    }else if ([UIScreen mainScreen].bounds.size.width == 320) {
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = 0 ;
//            _viewWidth.constant = 268;
//            x = self.viewWidth.constant -42 ;
//            [UIView animateWithDuration:0.1 animations:^{
//                _menuBtnLeading.constant = x ;
//                [self.view layoutIfNeeded];
//                
//            }];
//            
//            
//        }];
//        
//        
//    }
    
    [UIView animateWithDuration:0.5 animations:^{
                    _viewLeading.constant = 0 ;
                    _viewWidth.constant = [UIScreen mainScreen].bounds.size.width ;
//        CGRect frm = self.menuTableview.frame ;
//        frm.size.width = [UIScreen mainScreen].bounds.size.width;
//        self.menuTableview.frame = frm ;
        
        
                    x = [UIScreen mainScreen].bounds.size.width - 42 ;
                    [UIView animateWithDuration:0.1 animations:^{
                        _menuBtnLeading.constant = x ;
                        [self.view layoutIfNeeded];
        
                    }];
        
                    [self.view layoutIfNeeded];
                }];
    [self.slidingView bringSubviewToFront:_btnMenu];
    
    _dottedView.hidden =YES;
    
    
}


-(void)menuHide{
//    if ([UIScreen mainScreen].bounds.size.width == 375) {
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = -295 ;
//            _viewWidth.constant = -295 ;
//            x = 10 ;
//            _menuBtnLeading.constant = x ;
//            
//            [self.view layoutIfNeeded];
//            
//        }];
//        
//    }
//    else if ([UIScreen mainScreen].bounds.size.width == 414) {
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = -362;
//            _viewWidth.constant = -362;
//            x = 10 ;
//            _menuBtnLeading.constant = x ;
//            [self.view layoutIfNeeded];
//        }];
//        
//        
//    }else if ([UIScreen mainScreen].bounds.size.width == 320) {
//        [UIView animateWithDuration:0.5 animations:^{
//            _viewLeading.constant = -295 ;
//            _viewWidth.constant = -295;
//            x = 10;
//            _menuBtnLeading.constant = x ;
//            
//            [self.view layoutIfNeeded];
//        }];
//        
//    }
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    [UIView animateWithDuration:0.5 animations:^{
                    _viewLeading.constant = -[UIScreen mainScreen].bounds.size.width ;
                    _viewWidth.constant = -[UIScreen mainScreen].bounds.size.width ;
                    x = 10 ;
                    _menuBtnLeading.constant = x ;
        
                    [self.view layoutIfNeeded];
                    
                }];
    [_btnMenu setSelected:NO];
    
    NSString *str1 ;
    
     if (delegate.imagesArray.count>0) {
    if (pageNo!=(int)nil) {
        str1 = delegate.imagesArray[pageNo];
    }
    else{
        
        if (delegate.imagesArray.count >0) {
             str1 = delegate.imagesArray[0];
        }
       
    }
         
     }
    
    
    if ([str1 containsString:@"mp4"]) {
        [_btnTheme setHidden:YES];
        isPortrait = YES;
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setFromValue:[NSNumber numberWithFloat:0.0]];
        [animation setToValue:[NSNumber numberWithFloat:2.0]];
        [animation setDuration:.5f];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        [animation setRemovedOnCompletion:NO];
        [self.btnTheme.layer addAnimation:animation forKey:@"animation"];

        _txtTap.hidden = NO;
        
        
    }else{
        [_btnTheme setHidden:YES];
        isPortrait = NO;
        
          _txtTap.hidden = YES;
    }
    
    //_dottedView.hidden =NO;
}




-(void)sendScrollDescriptiontoBack{
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.saveBtn.alpha =1.0 ;
    
    [self.btnClose setHidden:YES];
    // [self.btnTheme setHidden:NO];
    [self.btnTheme setHidden:YES];
    [_imgCoin setHidden:NO];
    [_txtCoin setHidden:NO];
    _lbe_Time.hidden = NO;
    _lbe_Weekdays.hidden = NO;
     _bottomView.hidden = YES;
    _txtSwipe.hidden = NO;
    
    
    
    
    int  y = 0 ;
    [self.view bringSubviewToFront:_btnMenu];
    //[self.view bringSubviewToFront:_bottomView];
    [self.info_Btn setSelected:false];
    //  [_btnTheme setHidden:NO];
    [_btnTheme setHidden:YES];
    
    
    
    [UIView animateWithDuration:0.6
                          delay:0.2
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         self.scrllView.hidden= NO;
         
         
         [self.info_Btn setAlpha:1.0];
         
         
         self.scrllView.contentOffset = CGPointMake(0, y);
         _imgGradient.alpha = 0;
         
         [self.view layoutIfNeeded];
         
         self.scrllView.hidden= NO;
         _saveBtnBottom.constant = 40 ;
         
         
         self.saveBtn.alpha = 1.0;
         self.txtCoinsSave.alpha = 1.0;
         self.btnSaveSecond.alpha = 1.0;
         [self.view bringSubviewToFront:_btnSaveSecond];
         
         self.lbe_Time.alpha = 1.0;
         self.lbe_Weekdays.alpha = 1.0;
         
         
         self.imgSwipe.alpha =1.0 ;
         self.viewinfoHight.constant =0.0f;
         self.saveBtnBottomLayOut.constant = 17.0f;
         //  self.folderBtnLayOut.constant = 168.0f;
         self.setting_btnBottom.constant = 78.0f;
         self.viewLikeLayOut.constant = -202.0f;
         self.btnMenu.alpha = 1.0;
         self.btnMenu.alpha = 1.0;
         
         _saveBtnBottom.constant = 194 ;
         _heightCoinsLayout.constant = 205;
         
         [self.view layoutIfNeeded];
         _bottomView.backgroundColor = [UIColor clearColor];
         
         
         
     }
                     completion:^(BOOL finished)
     {
         [self.imgSwipe setHidden:YES];
         
         
         if(finished)
         {
             
             [self.saveBtn setHidden:NO];
         }
         
     }];
    
    
    [self.view sendSubviewToBack:self.scrllView];
    
    if (delegate.arrayKey.count == 0) {
        _saveBtn.alpha = 0.0;
        _txtCoinsSave.alpha = 0.0;
        _btnTheme.hidden = YES;
        _lbe_Time.hidden = YES;
        _lbe_Weekdays.hidden = YES;
         _bottomView.hidden = YES;
        _txtSwipe.hidden = YES;
        _txtTap.hidden = YES;
        
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 280,70)];
    sectionView.tag=section;
   viewLabel =[[UILabel alloc]initWithFrame:CGRectMake(20, 0, self.menuTableview.frame.size.width-10, 70)];
    sectionView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.9];

    viewLabel.textColor=[UIColor whiteColor];

    if (section == 0) {
        
        viewLabel.font= [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
         viewLabel.textColor  =[UIColor whiteColor];
       
    }
    else{
         viewLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        
    }
    if (section == 1) {
        viewLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        if (  [_strKey  isEqual: @"Live"]){
            
            
            if ([[menuItemsArray objectAtIndex:section] isEqualToString:@"Live Screens"]) {
                viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
                
                
                
            }else{
                
                viewLabel.textColor = [UIColor whiteColor];
                
                
            }
            
        }
       

    }
    
    
    if (section == 2) {
        viewLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        
        if (  [_strKey  isEqual: @"Featured"])
                {
                    if ([[menuItemsArray objectAtIndex:section]  isEqualToString:@"Featured"]) {
                       viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
                    }else{
            
                        viewLabel.textColor = [UIColor whiteColor];
                        
                        
                    }
                    
                }


    }
    
    if (section == 3) {
        viewLabel.font= [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        
          if (  [_strKey  isEqual: @"CrimsonCarrot"]){
              
              if ([[menuItemsArray objectAtIndex:section] isEqualToString:@"The Crimson Carrot"]) {
                  viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
                  
                  
                  
              }else{
                  
                  viewLabel.textColor = [UIColor whiteColor];
                  
                  
              }

              
          }
        
        
    }
    
    
    

    
        
    
       viewLabel.text=[NSString stringWithFormat:@"%@",[menuItemsArray objectAtIndex:section]];
    
    NSLog(@"%@",[menuItemsArray objectAtIndex:section]);
    
    CGSize size = [[menuItemsArray objectAtIndex:section] sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 30) lineBreakMode:NSLineBreakByWordWrapping];
    
    
    img = [[UIImageView alloc]initWithFrame:CGRectMake(viewLabel.frame.origin.x + size.width + 20 , 30, 15, 15)];
    // img.image = [UIImage imageNamed:@"sidearrow"];
    
    [sectionView addSubview:viewLabel];
    [sectionView addSubview:img];
    
    
    
    
    btn.titleLabel.font = [UIFont systemFontOfSize:20];
    
    [btn addTarget:self action:@selector(ButtonHeaderTappedClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = section;
    
    if (section == menuItemsArray.count -1){
        if (closeSection == true) {
            //[btn setImage:[UIImage imageNamed:@"sidearrow"] forState:UIControlStateNormal];
            img.image = [UIImage imageNamed:@"upIcon"];
             self.menuBottomView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.9];
        }
        else {
            
            img.image = [UIImage imageNamed:@"down-arrow (1)"];
            self.menuBottomView.backgroundColor = [UIColor clearColor];
            
            
            
        }
        
        
    }
    
    UITapGestureRecognizer *dropTableGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sectionHeaderTapped:)];
    sectionView.tag = section ;
    sectionView.userInteractionEnabled = YES;
    [dropTableGesture setNumberOfTouchesRequired:1];
    [dropTableGesture setDelegate:self];
    [sectionView addGestureRecognizer:dropTableGesture];
    
    
    return  sectionView;
}




- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (gestureRecognizer.view.tag == menuItemsArray.count - 1) {
        if (closeSection) {
            closeSection = false;
        }else{
            closeSection = true;
        }
    }
    else {
        
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        
        UITableViewCell * cell = [self.menuTableview cellForRowAtIndexPath:indexPath];
        
        
        
        for (id object in cell.superview.subviews) {
            if ([object isKindOfClass:[UITableViewCell class]]) {
                UITableViewCell *cellNotSelected = (UITableViewCell*)object;
                cellNotSelected.textLabel.textColor = [UIColor whiteColor];
            }
        }
        
//        cell.textLabel.textColor = [UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
        
        [_menuTableview deselectRowAtIndexPath:indexPath animated:NO];
        
        
        
        
        _imagesScrollView.contentOffset = CGPointZero;
        
        
        self.saveBtn.alpha =1.0 ;
        _txtCoinsSave.alpha = 1.0;
        self.viewNoThemes.hidden = YES;
        
        _imagesScrollView.hidden = NO;
        self.saveBtn.hidden = NO ;
        self.info_Btn.hidden = YES;
        _scrllView.scrollEnabled = YES;
        //_btnTheme.hidden = NO;
        _btnTheme.hidden = YES;
        
        _lbe_Time.hidden = NO;
        _lbe_Weekdays.hidden = NO;
        _bottomView.hidden = YES;
        _txtSwipe.hidden = NO;
        

        
        
        if (indexPath.section == 0) {
            _strKey = @"My collection" ;

            MyThemesController *my = [[ MyThemesController alloc]init];
            my = [self.storyboard instantiateViewControllerWithIdentifier:@"mythemes"];
            my.arrayImages = [NSMutableArray arrayWithArray:delegate.myCollectionArray];
            [self.navigationController pushViewController:my animated:YES];
            
            viewLabel.textColor = [UIColor whiteColor];
                                cell.textLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
            
             viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            

            
            
        }
        else if (indexPath.section == 1) {
            
            selectedIndex  = 1;
            
            
            viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
            delegate.imagesArray = [[NSMutableArray alloc]init];
            
            delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.liveKeys];
            self.strKey = @"Live";
            if (delegate.arrayKey.count >0) {
                
                
                delegate.isNoRecord = false ;
                
                
                NSLog(@"%@",delegate.imagesArray);
                
                
                for (NSMutableDictionary * data in delegate.liveKeys) {
                    NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                    if ([strImage containsString:@"mp4"]) {
                        strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                       withString:@""];
                        
                    }
                    
                    [delegate.imagesArray addObject:strImage];
                }
                
                [self getDescription];
                [self scrollimages];
                [self closeTableClicked:nil];
                [self getImagesFromServer];
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = YES;
                
                
            }
            else{
                delegate.isNoRecord = true;
                [self noThemesMethod];
                
            }

            
        }
        
        else if (indexPath.section == 2) {
            
            selectedIndex  = 2;
            
            viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
            
            delegate.imagesArray = [[NSMutableArray alloc]init];
            
            delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.featuredkeys];
            self.strKey = @"Featured";
            if (delegate.arrayKey.count >0) {
                
                
                delegate.isNoRecord = false ;
                
                NSLog(@"%@",delegate.imagesArray);
                for (NSMutableDictionary * data in delegate.featuredkeys) {
                    NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                    if ([strImage containsString:@"mp4"]) {
                        strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                       withString:@""];
                        
                    }
                    
                    [delegate.imagesArray addObject:strImage];
                }
                
                [self getDescription];
                [self scrollimages];
                [self closeTableClicked:nil];
                [self getImagesFromServer];
                
                _slider.hidden = YES;
                _landscapeSlider.hidden = YES;
                 viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
                
            }
            else{
                delegate.isNoRecord = true;
                [self noThemesMethod];
                
            }

            
            
        }
        else if (indexPath.section == 3) {
            
             selectedIndex  = 3;
            
            NSString *str = [SSKeychain passwordForService:@"plusThirteen" account:@"plusThirteen"];
            
            if ([str isEqualToString:@"YES"]) {
                viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
                
                delegate.imagesArray = [[NSMutableArray alloc]init];
                
                delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.crimpsonCarrotKeys];
                self.strKey = @"CrimsonCarrot";
                if (delegate.arrayKey.count >0) {
                    
                    
                    delegate.isNoRecord = false ;
                    
                    NSLog(@"%@",delegate.imagesArray);
                    for (NSMutableDictionary * data in delegate.crimpsonCarrotKeys) {
                        NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                        if ([strImage containsString:@"mp4"]) {
                            strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                           withString:@""];
                            
                        }
                        
                        [delegate.imagesArray addObject:strImage];
                    }
                    
                    NSLog(@"%@",delegate.imagesArray);
                    
                    
                    [self getDescription];
                    [self scrollimages];
                    [self closeTableClicked:nil];
                    [self getImagesFromServer];
                    
                    _slider.hidden = YES;
                    _landscapeSlider.hidden = YES;
                    
                     viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
                }
                else{
                    delegate.isNoRecord = true;
                    [self noThemesMethod];
                    
                }

            }else{
                
                _crimpsonFirstView.hidden = NO ;
                [self.view bringSubviewToFront:_crimpsonFirstView];
                
                
                [self menuHide];
                
            }
            
            
           
            
            
        }
        
        //            closeSection = true;
    }
    reloadTblVw = true;
    lastIndex = indexPath.section;
    [self.menuTableview reloadData];
}


-(IBAction)ButtonHeaderTappedClicked:(id)sender
{
    
    UIButton *button = (UIButton* )sender;
    NSInteger bTn = button.tag;
    
    NSLog(@"%ld",(long)bTn);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:bTn];
    
    if (reloadTblVw) {
        if (lastIndex == bTn && !closeSection) {
            closeSection = true;
        }
        else {
            closeSection = false;
        }
    }
    else {
        closeSection = false;
    }
    reloadTblVw = true;
    lastIndex = indexPath.section;
    
    [self.menuTableview reloadData];
    
    //    if (!closeSection) {
    //
    //        [self.menuTableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:lastIndex] atScrollPosition:UITableViewScrollPositionTop animated:true];
    //    }
    
}

#pragma mark
#pragma mark UIScrollView Delegate Methods
#pragma mark

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _lastContentOffset = _imagesScrollView.contentOffset;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self.btnTheme setBackgroundImage:[UIImage imageNamed:@"livephoto-icon.png"] forState:UIControlStateNormal];
    
    _saveBtn.hidden = NO;
    if (scrollView== _imagesScrollView) {
        _imagesScrollView.scrollEnabled = YES;
        
        pageNo = round(_imagesScrollView.contentOffset.x / _imagesScrollView.frame.size.width);
        NSLog(@"Page number is %i", pageNo);
        checkPortrait = true ;
        
        
        
    }
    
    
    NSString *str ;
    
    if (delegate.arrayKey.count >0) {
        if (pageNo != (int)nil) {
            strId = [NSString stringWithFormat:@"%@",[[delegate.arrayKey  valueForKey:@"id"] objectAtIndex:pageNo]];
            NSLog(@"%@",strId);
            
            str  = [delegate.imagesArray objectAtIndex:pageNo];
        }else{
            
            
            str  = [delegate.imagesArray objectAtIndex:0];
        }

    }
    
    
    
    
    
    if ([str containsString:@"mp4"]) {
        
        if (isPortrait == true) {
            self.slider.hidden = NO;
            self.landscapeSlider.hidden = YES;
        }else{
            self.slider.hidden = YES;
            self.landscapeSlider.hidden = NO;
            
        }
        
        
        [_btnTheme setHidden:YES];
        [_btnTheme setUserInteractionEnabled:YES];
        [self.view bringSubviewToFront:self.btnTheme];
        
        _slider.value = 0.0 ;
        _landscapeSlider.value = 0.0 ;
        [self.view bringSubviewToFront:self.btnMenu];
        
        _txtTap.hidden = NO;

        
        
    }else{
        
        [_btnTheme setHidden:YES];
        _slider.hidden = YES;
        _landscapeSlider.hidden = YES;
        
        _txtTap.hidden = YES;

        
    }
    
    
    
    //        UIImageView *SpecificTheme = [[UIImageView alloc]init];
    //        SpecificTheme.image = [UIImage imageNamed:[delegate.imagesArray objectAtIndex:pageNo]];
    //
    
    
    if (delegate.arrayKey.count == 0) {
        _saveBtn.hidden = YES;
        
        
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (pageNo == 16) {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        
        
        AVPlayerItem *currentItem = avPlayer.currentItem;
        
        NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
        NSTimeInterval totalTime = CMTimeGetSeconds(currentItem.duration);
        NSLog(@" Capturing Time :%f ",currentTime);
        
        float totalduration = totalTime ;
        float currentduration = currentTime ;
        
        float durationLeft = totalduration - currentduration ;
        
        
        
        
    }
     if (delegate.arrayKey.count >0) {
    if (pageNo!= (int)nil) {
        videoStr = delegate.imagesArray[pageNo];
        NSLog(@"Page NUmber %d",pageNo);
    }
    else{
        
        videoStr = delegate.imagesArray[0];
    }
     }
    
    
    
    
    //    NSArray* foo = [videoStr componentsSeparatedByString: @"."];
    //    NSString* namee = [foo objectAtIndex: 0];
    //    NSString*type = [foo objectAtIndex:1];
    
    NSLog(@"Video Str %@",videoStr);
    if ([videoStr containsString:@"mp4"]) {
    
        [avPlayer pause];
        
        if (isPortrait == true) {
            self.slider.hidden = NO;
            self.landscapeSlider.hidden = YES;
        }else{
        self.slider.hidden = YES;
        self.landscapeSlider.hidden = NO;
        
        }
        self.btnTheme.userInteractionEnabled = YES;
        
        _slider.hidden = NO;
        
        _currentVideoTime.hidden = NO;
        [_btnTheme setHidden:YES];
        // [self scrollimages];
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setFromValue:[NSNumber numberWithFloat:0.0]];
        [animation setToValue:[NSNumber numberWithFloat:2.0]];
        [animation setDuration:.5f];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        [animation setRemovedOnCompletion:NO];
        [self.btnTheme.layer addAnimation:animation forKey:@"animation"];
        
        // [self.av_player play];
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[avPlayer currentItem]];
        
        goMyThemView = YES;
        
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate setShouldRotate:NO];
        //        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
        //        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        
        
        [avPlayer seekToTime:CMTimeMake(0, 1)];
        [avPlayer pause];
        [self.btnTheme setSelected:NO];
        
        _txtTap.hidden = NO;

        
    }else{
        
        
        _btnTheme.hidden = YES;
        _slider.hidden = YES;
        _landscapeSlider.hidden = YES;
        
        _currentVideoTime.hidden = YES;
        [_btnTheme setHidden:YES];
        goMyThemView = NO;
        // [self.av_player pause];
        [self.view bringSubviewToFront:self.imageView];
        
        [_btnTheme.layer removeAllAnimations];
        
        _txtTap.hidden = YES;

        
        
    }
    
    
    if (_lastContentOffset.x < (int)_imagesScrollView.contentOffset.x) {
        NSLog(@"Scrolled Right");
        
        rightDraggingCount = rightDraggingCount + 1 ;
        
        
        [[NSUserDefaults standardUserDefaults]setInteger:rightDraggingCount forKey:@"rightCount"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    
    if (rightDraggingCount == 16){
        self.adLbl.hidden = NO ;
        self.btnAd.hidden = YES ;
        self.AdView.hidden = NO;
        [self adskipTimer];
        rightDraggingCount  = 0;
        [self.btnMenu setHidden:true];
        
        [[NSUserDefaults standardUserDefaults]setInteger:rightDraggingCount forKey:@"rightCount"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self.view bringSubviewToFront:self.AdView];
        
        [self adGoogle];
        
        [self.view bringSubviewToFront:_AdView];
        
        
    }
    
    [avPlayer seekToTime:CMTimeMake(0, 1)];
    [avPlayer pause];
    [self.btnTheme setSelected:NO];
    
    
    if (scrollView == _menuTableview) {
        [self.btnTheme setHidden:YES];
        
    }else{
        
        [self postRequestToTrack_View];
        
    }
    
    
}

-(void)adGoogle{
    
    self.bannerVieww.delegate = self;
    
    self.bannerVieww.adUnitID =@"ca-app-pub-5604206830822766/2267569735";
    self.bannerVieww.rootViewController = self;
    [self.bannerVieww loadRequest:[GADRequest request]];
    
    
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (IBAction)getStartedClicked:(id)sender {
    
    
}
- (IBAction)nextClicked:(id)sender {
    self.viewGetStarted.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = NO;
    self.viewFourth.hidden = YES;
    //_btnTheme.hidden = NO;
    _btnTheme.hidden = YES;
}
- (IBAction)nextSecondClicked:(id)sender {
    self.viewGetStarted.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = NO;
    
    
}
- (IBAction)nextThirdClicked:(id)sender {
    self.viewGetStarted.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFirstTime.hidden = YES;
    _btnTheme.hidden = YES;
    self.lblFourth.hidden = NO;
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"firstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(void)CategoryDataFromServer {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //        [KVNProgress dismiss];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message: @"No Network Connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"https://artlords.com/api/theme_collections"];
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strUrl]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
        
        [request setValue:@"Token token=f7e31f33930d0351f99e09d81e708b51e906c3eb34d8af71" forHTTPHeaderField:@"Authorization"];
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        
        [request setHTTPMethod:@"GET"];
        
        
        
        
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            request.cachePolicy = NSURLRequestReloadIgnoringCacheData ;
            if (data!=nil) {
                
                
                categoryData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSLog(@"Category Data: %@", categoryData);
                
                collectionsArray = [[NSMutableArray alloc]init];

                for (NSDictionary*dict in categoryData) {
                    [ collectionsArray addObject:dict[@"name"]];
                }
                
                NSLog(@"collections Array: %@",collectionsArray);
                
                
                [self.menuTableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                
                
            }
        }];
        [dataTask resume];
        
        
    }
    
    
}



-(void)getImagesFromServer {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //        [KVNProgress dismiss];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message: @"No Network Connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        //[alert show];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"https://artlords.com/api/themes/%@",strId];
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strUrl]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
        
        [request setValue:@"Token token=f7e31f33930d0351f99e09d81e708b51e906c3eb34d8af71" forHTTPHeaderField:@"Authorization"];
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        
        [request setHTTPMethod:@"GET"];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            request.cachePolicy = NSURLRequestReloadIgnoringCacheData ;
            if (data!=nil) {
                
                
                dataJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSLog(@"data JSON: %@", dataJSON);
            }
            
            
            if (saveClicked == YES) {
                [self performSelectorOnMainThread:@selector(detailsData) withObject:nil waitUntilDone:YES];
            }
            
            
        }];
        [dataTask resume];
        
        
    }
    
    
}


-(void)postRequestToTrack_View{
    
    NSLog(@"%@",strId);
    
    
    NSString * apiURLStr = [NSString stringWithFormat:@"https://artlords.com/api/themes/%@/track_view",strId];
    
    NSMutableURLRequest *urlrequest=[[NSMutableURLRequest alloc]init];
    
    
    
    [urlrequest setURL:[NSURL URLWithString:apiURLStr]];
    [urlrequest setHTTPMethod:@"POST"];
    
    [urlrequest setValue:@"Token token=f7e31f33930d0351f99e09d81e708b51e906c3eb34d8af71" forHTTPHeaderField:@"Authorization"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlrequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (data ==nil) {
                                          
                                      }else{
                                          trackviewData=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                                          NSLog(@"%@",trackviewData);
                                          
                                      }
                                      
                                  }];
    
    
    [task resume];
    
}


-(void)detailsData{
    
    self.infoThemeName.text = @"";
    self.infoCollectionName.text = @"";
    self.infoDescription.text = @"";
    
    if ([dataJSON valueForKey:@"name"] == (id)[NSNull null] || [dataJSON valueForKey:@"name"] == nil){
        
        
    }else{
        
        self.infoThemeName.text = [dataJSON valueForKey:@"name"];
    }
    
    
    if ([dataJSON valueForKey:@"category"] == (id)[NSNull null] || [dataJSON valueForKey:@"category"] == nil) {
        
        
    }else{
        self.infoCollectionName.text = [dataJSON valueForKey:@"category"];
        
        
        //         for ( NSString *strr in self.infoCollectionName.text) {
        //
        //
        //         }
        
        NSLog(@"%@",collectionName);
        
        
        
        
        
    }
    NSLog(@"%d",pageNo);
    
   
    
    
    if ([dataJSON valueForKey:@"description"] == (id)[NSNull null] || [dataJSON valueForKey:@"description"] == nil) {
        
    }else{
        
        self.infoDescription.text = [dataJSON valueForKey:@"description"];
    }
    
    if ([dataJSON valueForKey:@"points"] == (id)[NSNull null] || [dataJSON valueForKey:@"points"] == nil) {
        
    }else{
        
        pointsNeeded = [dataJSON valueForKey:@"points"];
    }
    
    
    
    
    [self.view bringSubviewToFront:self.saveBtn];
    
    _scrllView.hidden = YES;
    
    self.viewInfoThemeSave.hidden = NO;
    [self.view bringSubviewToFront:self.viewInfoThemeSave];
    
    
    [self.btnClose setHidden:YES];
    // [self.bottomView setHidden:YES];
    //  [self.view bringSubviewToFront:self.viewFirstTime];
    
}

- (IBAction)NextFirstClicked:(id)sender {
    
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = NO;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFive.hidden = YES;
    self.viewSix.hidden = YES;
    self.viewSeven.hidden = YES;
    
    self.leftLblFirst.hidden = YES;
    self.rightLabelFirst.hidden = YES;
    
    
    [self.view bringSubviewToFront:_saveBtn];
    
    
    
    
    
}
- (IBAction)nextSixthClicked:(id)sender {
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFive.hidden = YES;
    self.viewSix.hidden = YES;
    self.viewSeven.hidden = NO;
    self.lblSixth.hidden = YES;
    
    
    [self.view bringSubviewToFront:_viewFirstTime];
    
    
    [self.view bringSubviewToFront:self.btnMenu];
    [self.view bringSubviewToFront:self.btnCoins];
    [self.btnMenu setEnabled:YES];
    
    self.lblSeventh.hidden = NO;
    
    
    
}

- (IBAction)btnNextFifth:(id)sender {
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFive.hidden = YES;
    self.viewSix.hidden = NO;
    self.viewSeven.hidden = YES;
    
    self.lblSixth.hidden = NO;
    self.lblSeventh.hidden = YES;
    self.lblFifth.hidden = YES;
    
    //   [_pointsUsed setHidden: YES ];
    
    
    [self.view bringSubviewToFront:self.viewRed];
    [self.view bringSubviewToFront:self.viewFirstTime];
    [self.view bringSubviewToFront:self.btnCoins];
}

- (IBAction)NextFourthClicked:(id)sender {
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFive.hidden = NO;
    self.viewSix.hidden = YES;
    self.viewSeven.hidden = YES;
    self.btnTheme.hidden = YES;
    self.lblFifth.hidden = NO;
    self.lblFourth.hidden = YES;
    
    
    if ([UIScreen mainScreen].bounds.size.width == 320) {
        _YViewFive.constant =  80 ;
        [self.view layoutIfNeeded];
    }else{
        
        _YViewFive.constant =  125.5 ;
        [self.view layoutIfNeeded];
    }
    
    
     self.txtTap.hidden = NO;
    
    _txtNewBalance.textColor = [UIColor whiteColor];
    _txtBalance.textColor = [UIColor whiteColor];
    _txtPoints.textColor = [UIColor whiteColor];
    _btnGetMore.enabled = YES;
    _doneClickedd.enabled = YES;
    //
    [_btnGetMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_doneClickedd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    _btnCloseee.enabled = YES;
    
    _viewCoinsss.layer.borderWidth = 3.0 ;
    
    // [self.view bringSubviewToFront:self.viewFirstTime];
    
    [self.view bringSubviewToFront:self.ViewSaved];
    [self.view bringSubviewToFront:self.viewFourth];
    
    [self.view bringSubviewToFront:self.btnNextFourth];
    [self.btnNextFourth setEnabled:YES];
    
    
    [self.view bringSubviewToFront:self.btnTheme];
    self.btnTheme.userInteractionEnabled = NO;
    
    [self.view bringSubviewToFront:self.viewFirstTime];
    [self.viewFirstTime setHidden:NO];
    
    [self.view bringSubviewToFront:self.btnTheme];
    [self.view bringSubviewToFront:self.txtTap];

//    [self.view bringSubviewToFront:self.txtSwipe];

    [self.ViewSaved setHidden:YES];
    
    
}

- (IBAction)NextSeventhClicked:(id)sender {
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.viewFirst.hidden = YES;
    self.viewSecond.hidden = YES;
    self.viewThird.hidden = YES;
    self.viewFourth.hidden = YES;
    self.viewFive.hidden = YES;
    self.viewSix.hidden = YES;
    self.viewSeven.hidden = YES;
    
    self.lblSeventh.hidden = YES;
    //   self.lblSixth.hidden = YES;
    self.lblFifth.hidden = YES;
    self.leftLblFirst.hidden = YES;
    self.rightLabelFirst.hidden = YES;
    
    
    [self.viewFirstTime setHidden:YES];
    
    
    self.infoCloseThemeee.enabled = YES;
    self.btnMenu.enabled = YES;
    self.btnMenu.enabled = YES;
    _themeCloseBtnn.enabled = YES;
    _btnCoins.enabled = YES;
    _btnLandscapeCoins.enabled = YES;
    _btnTheme.enabled = YES;
    
    [self.ViewSaved setHidden:YES];
    _btnMenu.userInteractionEnabled = YES;
    [_btnTheme setUserInteractionEnabled:YES];
    
    [_txtTap.layer removeAllAnimations];
    
    
    
    _infoCloseThemeee.titleLabel.textColor = [UIColor whiteColor];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"orientation"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.view bringSubviewToFront:self.viewRed];
    
        NSString *str1  = [delegate.imagesArray objectAtIndex:pageNo];
    
    if ([str1 containsString:@"mp4"]) {
        [_btnTheme setHidden:YES];
        isPortrait = YES;
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setFromValue:[NSNumber numberWithFloat:0.0]];
        [animation setToValue:[NSNumber numberWithFloat:2.0]];
        [animation setDuration:.5f];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        [animation setRemovedOnCompletion:NO];
        [self.btnTheme.layer addAnimation:animation forKey:@"animation"];
        
        
        
    }else{
        [_btnTheme setHidden:YES];
        isPortrait = NO;
    }
   
    [_btnTheme setHidden:YES];
    
    //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"artlords://deeplink/page1"]];
    
    self.imagesScrollView.scrollEnabled = YES;
}


-(void)playBtn{
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSString *str1 ;
        if (delegate.imagesArray.count>0) {
             str1  = [delegate.imagesArray objectAtIndex:pageNo];
        }
       
        
        if ([str1 containsString:@"mp4"]) {
            [_btnTheme setHidden:YES];
            isPortrait = YES;
            animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
            [animation setFromValue:[NSNumber numberWithFloat:0.0]];
            [animation setToValue:[NSNumber numberWithFloat:2.0]];
            [animation setDuration:.5f];
            [animation setRepeatCount:100];
            [animation setAutoreverses:YES];
            [animation setRemovedOnCompletion:NO];
            [self.btnTheme.layer addAnimation:animation forKey:@"animation"];
            
            
        }else{
            [_btnTheme setHidden:YES];
            isPortrait = NO;
        }

    }

-(void)layoutBtns{
    
    
    [_btnCoins setTitle:@"250" forState:UIControlStateNormal];
    CGFloat fontSize = self.btnCoins.titleLabel.font.pointSize;
//    self.btnCoins.titleLabel.font = [UIFont systemFontOfSize:fontSize-2];
    [_btnLandscapeCoins setTitle:@"250" forState:UIControlStateNormal];
//    _btnCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//    _btnLandscapeCoins.titleLabel.adjustsFontSizeToFitWidth = YES;
//    self.btnCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;
//    self.btnLandscapeCoins.titleLabel.lineBreakMode = NSLineBreakByClipping;

 //  [SSKeychain deletePasswordForService:@"image" account:@"imagelordData"];
    
    NSString *strKeychain =[SSKeychain passwordForService:@"image" account:@"imagelordData"];
    
    if (strKeychain!=nil && strKeychain != (id)[NSNull null]) {
        
        if (![strKeychain isEqualToString:@"(null)"]) {
            NSString *coins = [SSKeychain passwordForService:@"image" account:@"imagelordData"];
            NSLog(@"%@",coins);
            
            _btnCoins.titleLabel.text = coins ;
            _btnLandscapeCoins.titleLabel.text = coins ;
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"keychain"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"orientation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            
        }
        
    }else{
        
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"keychain"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"orientation"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
    self.btnGetMore.titleLabel.numberOfLines = 1;
    self.btnGetMore.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.btnGetMore.titleLabel.lineBreakMode = NSLineBreakByClipping;
    self.themesVieww.layer.shadowOpacity = 0.6;
    
    
    
    
    _viewCoinsss.backgroundColor = [UIColor colorWithRed:10/255.0f green:8/255.0f  blue:7/255.0f  alpha:0.9];
    _viewCoinsss.layer.borderColor = [UIColor colorWithRed:166/255.0f green:165/255.0f  blue:164/255.0f  alpha:0.8].CGColor;
    _viewCoinsss.layer.borderWidth=3.0f;
    self.viewCoinsss.layer.masksToBounds = NO;
    self.viewCoinsss.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewCoinsss.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewCoinsss.layer.shadowOpacity = 0.6f;
    self.viewCoinsss.layer.shadowPath = shadowPath.CGPath;
    
    
    _btnCoins.layer.cornerRadius = 5;//half of the width
    _btnCoins.layer.borderColor=[UIColor colorWithRed:216/255.0f green:162/255.0f  blue:40/255.0f  alpha:1].CGColor;
    _btnCoins.layer.borderWidth=2.0f;
    
    
    _btnLandscapeCoins.layer.cornerRadius = 5;
    _btnLandscapeCoins.layer.borderColor=[UIColor colorWithRed:216/255.0f green:162/255.0f  blue:40/255.0f  alpha:1].CGColor;
    _btnLandscapeCoins.layer.borderWidth=2.0f;
    
    
    
    _menuTableview.separatorColor =[UIColor colorWithRed:216/255.0f green:162/255.0f  blue:40/255.0f  alpha:1];
    
    
    _infoPointsUsed.backgroundColor = [UIColor colorWithRed:238/255.0f green:179/255.0f  blue:21/255.0f  alpha:0.5];
    
    _infoPointsUsed.layer.borderColor =[UIColor colorWithRed:238/255.0f green:179/255.0f  blue:21/255.0f  alpha:1].CGColor;
    
    _infoPointsUsed.layer.borderWidth=2.0f;
    _infoPointsUsed.layer.cornerRadius = 10.0f;
    
    
    
    _themesVieww.backgroundColor = [UIColor colorWithRed:10/255.0f green:8/255.0f  blue:7/255.0f  alpha:0.9];
    //_themesVieww.layer.borderColor =[UIColor colorWithRed:238/255.0f green:179/255.0f  blue:21/255.0f  alpha:0.8].CGColor;
    _themesVieww.layer.borderColor =[UIColor colorWithRed:166/255.0f green:165/255.0f  blue:164/255.0f  alpha:0.8].CGColor;
    _themesVieww.layer.borderWidth=3.0f;
    
    self.themesVieww.layer.masksToBounds = NO;
    self.themesVieww.layer.shadowColor = [UIColor blackColor].CGColor;
    self.themesVieww.layer.shadowOffset = CGSizeMake(0.9f, 0.9f);
    self.themesVieww.layer.shadowOpacity = 0.6f;
    self.themesVieww.layer.shadowPath = shadowPath.CGPath;
    
    
    _infoSaveTheme.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _infoSaveTheme.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _infoSaveTheme.layer.borderWidth=3.0f;
    self.infoSaveTheme.layer.masksToBounds = NO;
    self.infoSaveTheme.layer.shadowColor = [UIColor blackColor].CGColor;
    self.infoSaveTheme.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.infoSaveTheme.layer.shadowOpacity = 0.6f;
    self.infoSaveTheme.layer.shadowPath = shadowPath.CGPath;
    
    
    
    _infoCloseThemeee.backgroundColor = [UIColor colorWithRed:44/255.0f green:55/255.0f  blue:64/255.0f  alpha:0.8];
    _infoCloseThemeee.layer.borderColor =[UIColor colorWithRed:166/255.0f green:165/255.0f  blue:164/255.0f  alpha:0.8].CGColor;
    
    
    
    _infoCloseThemeee.layer.borderWidth=1.0f;
    self.infoCloseThemeee.layer.masksToBounds = NO;
    self.infoCloseThemeee.layer.shadowColor = [UIColor blackColor].CGColor;
    self.infoCloseThemeee.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.infoCloseThemeee.layer.shadowOpacity = 0.6f;
    self.infoCloseThemeee.layer.shadowPath = shadowPath.CGPath;
    // _txtNewBalance.backgroundColor = [UIColor colorWithRed:156/255.0f green:133/255.0f  blue:54/255.0f  alpha:0.3];
    
    _txtPoints.backgroundColor = [UIColor colorWithRed:44/255.0f green:48/255.0f  blue:45/255.0f  alpha:0.8];
    _txtNewBalance.backgroundColor = [UIColor colorWithRed:44/255.0f green:48/255.0f  blue:45/255.0f  alpha:0.8];
    _txtUsedCoins.backgroundColor = [UIColor colorWithRed:44/255.0f green:48/255.0f  blue:45/255.0f  alpha:0.8];
    _txtBalance.backgroundColor = [UIColor colorWithRed:44/255.0f green:48/255.0f  blue:45/255.0f  alpha:0.8];
    
    
    
    _btnGetMore.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnGetMore.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnGetMore.layer.borderWidth=3.0f;
    self.btnGetMore.layer.masksToBounds = NO;
    self.btnGetMore.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnGetMore.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.btnGetMore.layer.shadowOpacity = 0.6f;
    self.btnGetMore.layer.shadowPath = shadowPath.CGPath;
    
    
    
    _addPointsView.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _addPointsView.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    
    
    _addPointsView.layer.borderWidth=3.0f;
    self.addPointsView.layer.masksToBounds = NO;
    self.addPointsView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.addPointsView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.addPointsView.layer.shadowOpacity = 0.6f;
    self.addPointsView.layer.shadowPath = shadowPath.CGPath;
    
    
    //  _doneClickedd.backgroundColor = [UIColor colorWithRed:47/255.0f green:43/255.0f  blue:28/255.0f  alpha:0.8];
    _doneClickedd.backgroundColor = [UIColor colorWithRed:46/255.0f green:55/255.0f  blue:64/255.0f  alpha:0.9];
    _doneClickedd.layer.borderColor =[UIColor colorWithRed:166/255.0f green:165/255.0f  blue:164/255.0f  alpha:0.8].CGColor;
    _doneClickedd.layer.borderWidth=1.0f;
    self.doneClickedd.layer.masksToBounds = NO;
    self.doneClickedd.layer.shadowColor = [UIColor blackColor].CGColor;
    self.doneClickedd.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.doneClickedd.layer.shadowOpacity = 0.6f;
    self.doneClickedd.layer.shadowPath = shadowPath.CGPath;
    
    
    /////creating first time views with shadows
    
    _viewFirstTime.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f  blue:0/255.0f  alpha:0.4];
    _viewThird.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f  blue:0/255.0f  alpha:0.6];
    
    //firstvIEW
    self.viewFirst.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f  blue:255/255.0f  alpha:0.7];
    self.viewFirst.layer.masksToBounds = NO;
    self.viewFirst.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewFirst.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewFirst.layer.shadowOpacity = 1.0f;
    self.viewFirst.layer.shadowPath = shadowPath.CGPath;
    _viewFirst.layer.borderColor= [UIColor grayColor].CGColor;
    _viewFirst.layer.borderWidth=0.5f;
    
    
    
    
    
    _btnNextFirst.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnNextFirst.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnNextFirst.layer.borderWidth=3.0f;
    _btnNextFirst.layer.masksToBounds = NO;
    _btnNextFirst.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnNextFirst.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _btnNextFirst.layer.shadowOpacity = 0.6f;
    _btnNextFirst.layer.shadowPath = shadowPath.CGPath;
    
    
    //fourthvIEW
    
    
    
    self.viewFourth.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f  blue:255/255.0f  alpha:0.8];
    self.viewFourth.layer.masksToBounds = NO;
    self.viewFourth.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewFourth.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewFourth.layer.shadowOpacity = 1.0f;
    self.viewFourth.layer.shadowPath = shadowPath.CGPath;
    _viewFourth.layer.borderColor= [UIColor grayColor].CGColor;
    _viewFourth.layer.borderWidth=1.0f;
    
    _btnNextFourth.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnNextFourth.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnNextFourth.layer.borderWidth=3.0f;
    _btnNextFourth.layer.masksToBounds = NO;
    _btnNextFourth.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnNextFourth.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _btnNextFourth.layer.shadowOpacity = 0.6f;
    _btnNextFourth.layer.shadowPath = shadowPath.CGPath;
    
    //fifthvIEW
    self.viewFive.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f  blue:255/255.0f  alpha:0.7];
    self.viewFive.layer.masksToBounds = NO;
    self.viewFive.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewFive.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewFive.layer.shadowOpacity = 1.0f;
    self.viewFive.layer.shadowPath = shadowPath.CGPath;
    _viewFive.layer.borderColor= [UIColor grayColor].CGColor;
    _viewFive.layer.borderWidth=1.0f;
    
    _btnNextFifth.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnNextFifth.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnNextFifth.layer.borderWidth=3.0f;
    _btnNextFifth.layer.masksToBounds = NO;
    _btnNextFifth.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnNextFifth.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _btnNextFifth.layer.shadowOpacity = 0.6f;
    _btnNextFifth.layer.shadowPath = shadowPath.CGPath;
    
    
    //SIXTHvIEW
    self.viewSix.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f  blue:255/255.0f  alpha:0.7];
    self.viewSix.layer.masksToBounds = NO;
    self.viewSix.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewSix.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewSix.layer.shadowOpacity = 1.0f;
    self.viewSix.layer.shadowPath = shadowPath.CGPath;
    _viewSix.layer.borderColor= [UIColor grayColor].CGColor;
    _viewSix.layer.borderWidth=1.0f;
    
    _btnNextSixth.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnNextSixth.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnNextSixth.layer.borderWidth=3.0f;
    _btnNextSixth.layer.masksToBounds = NO;
    _btnNextSixth.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnNextSixth.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _btnNextSixth.layer.shadowOpacity = 0.6f;
    _btnNextSixth.layer.shadowPath = shadowPath.CGPath;
    
    
    
    //SEVENTHvIEW
    self.viewSeven.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f  blue:255/255.0f  alpha:0.7];
    self.viewSeven.layer.masksToBounds = NO;
    self.viewSeven.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewSeven.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.viewSeven.layer.shadowOpacity = 1.0f;
    self.viewSeven.layer.shadowPath = shadowPath.CGPath;
    _viewSeven.layer.borderColor= [UIColor grayColor].CGColor;
    _viewSeven.layer.borderWidth=1.0f;
    
    _btnNextSeventh.backgroundColor = [UIColor colorWithRed:22/255.0f green:153/255.0f  blue:90/255.0f  alpha:0.8];
    _btnNextSeventh.layer.borderColor =[UIColor colorWithRed:24/255.0f green:188/255.0f  blue:109/255.0f  alpha:0.8].CGColor;
    _btnNextSeventh.layer.borderWidth=3.0f;
    _btnNextSeventh.layer.masksToBounds = NO;
    _btnNextSeventh.layer.shadowColor = [UIColor blackColor].CGColor;
    _btnNextSeventh.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _btnNextSeventh.layer.shadowOpacity = 0.6f;
    _btnNextSeventh.layer.shadowPath = shadowPath.CGPath;
    
    [self.lblTime setNumberOfLines:1];
    [self.lblTime setAdjustsFontSizeToFitWidth:YES];
    
    [self.txtPointsRemain setNumberOfLines:1];
    [self.txtPointsRemain setAdjustsFontSizeToFitWidth:YES];
    
    [self.txtPointsUsed setNumberOfLines:1];
    [self.txtPointsUsed setAdjustsFontSizeToFitWidth:YES];
    
    [self.txtAdded setNumberOfLines:2];
    [self.txtAdded setAdjustsFontSizeToFitWidth:YES];
    
    
    [self updateTime];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(updateTime)
                                   userInfo:nil
                                    repeats:YES];
    
}

-(void)addPortraitLandscapeScrollView{
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
    {
        
        _sliderTop.constant  = 2 ;
        _imagesScrollView.frame = CGRectMake(-1, 0,  self.view.frame.size.width +1,  self.view.frame.size.height);
        _imagesScrollView.scrollEnabled = YES;
        _btnMenu.hidden = NO ;
        _viewRed.hidden = YES;
        _btnCoins.hidden = NO ;
        
        _btnLandscapeCoins.hidden = YES;
        _imgLock.hidden = YES;
        
        _txtSwipe.hidden = NO;
        
        
        [_themeNamee setFont: [_themeNamee.font fontWithSize: 32]];
        [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 20]];
        [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 32]];
        
        _widthCoinLayout.constant = 40 ;
        _imgwidthLayout.constant = 60 ;
        _yLayout.constant = 1.53;
        
        [_lbe_Time.layer removeAllAnimations];
        [_lbe_Weekdays.layer removeAllAnimations];
        _lbe_Time.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
        _lbe_Weekdays.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
        
        _bottomViewSecond.hidden = YES;
        _bottomView.hidden = NO;
         _bottomView.hidden = YES;
        
        
        
    }
    else  if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft)  )
    {
        
        _sliderTop.constant  = -self.view.frame.size.height+5;
        _imagesScrollView.frame = CGRectMake(-1, 0, self.view.frame.size.width, self.view.frame.size.height+1);
        
        _imagesScrollView.scrollEnabled = NO;
        _btnMenu.hidden = YES ;
        _viewRed.hidden = YES;
        _btnCoins.hidden = YES ;
        _btnLandscapeCoins.hidden = NO;
        _imgLock.hidden = NO;
        
        _txtSwipe.hidden = YES;
        
        
        
        [_themeNamee setFont: [_themeNamee.font fontWithSize: 24]];
        [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 16]];
        [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 24]];
        
        _widthCoinLayout.constant = 30 ;
        _imgwidthLayout.constant = 35 ;
        _yLayout.constant = 5.0;
        
        
        _lbe_Time.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        _lbe_Weekdays.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        
        _bottomViewSecond.hidden = YES;
        _bottomView.hidden = YES;
        _bottomView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        
        
        
        
    }
    else  if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight)  )
    {
        
        // _sliderTop.constant  = -15 ;
        _imagesScrollView.frame = CGRectMake(-1, 0, self.view.frame.size.width, self.view.frame.size.height+1);
        _imagesScrollView.scrollEnabled = NO;
        _btnMenu.hidden = YES ;
        _viewRed.hidden = YES;
        _btnCoins.hidden = YES ;
        _btnLandscapeCoins.hidden = NO;
        _imgLock.hidden = NO;
        
        _txtSwipe.hidden = YES;
        [_themeNamee setFont: [_themeNamee.font fontWithSize: 24]];
        [_TxtThemeAdded setFont: [_TxtThemeAdded.font fontWithSize: 14]];
        [_infoThemeName setFont: [_infoThemeName.font fontWithSize: 24]];
        _widthCoinLayout.constant = 30 ;
        _imgwidthLayout.constant = 35 ;
        _yLayout.constant = 5.0;
        
        
        _lbe_Time.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        _lbe_Weekdays.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        
        _bottomViewSecond.hidden = YES;
        _bottomView.hidden = YES;
        _bottomView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        
        
        
    }
    
   
    
    [_imagesScrollView setContentOffset:CGPointMake(_imagesScrollView.frame.size.width*pageNo, 0)];
    
    [self.view addSubview:_imagesScrollView];
    
    [self scrollimages];
    
    
    
}




-(void)dottBtn{
    
    self.dottedView.layer.cornerRadius = self.dottedView.frame.size.width/2;
    
    [self.view clipsToBounds];
    
    CAShapeLayer * dotborder = [CAShapeLayer layer];
    dotborder.strokeColor = [UIColor grayColor].CGColor;//your own color
    dotborder.fillColor = nil;
    dotborder.lineDashPattern = @[@2, @1];//your own patten
    [_dottedView.layer addSublayer:dotborder];
    dotborder.path = [UIBezierPath bezierPathWithOvalInRect:_dottedView.bounds].CGPath;
    dotborder.frame = _dottedView.bounds;
}

//
//-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
//
//
//
////      [self.av_player play];
//}


-(void)rotationOfView{
    
    
    [self rotationMethod];
    
    if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
    {
        _topTimeLayout.constant = 30 ;
        [self.view layoutIfNeeded];
        
        _topDaysLayout.constant = 104 ;
        [self.view layoutIfNeeded];
        
        
        _time_X_Pos.constant = 0 ;
        [self.view layoutIfNeeded];
        
        _day_X_pos.constant = 0 ;
        [self.view layoutIfNeeded];
        
        _save_Y_pos.constant = 53.5;
        [self.view layoutIfNeeded];
        
        _bottomView.transform=CGAffineTransformMakeRotation( ( 180 * M_PI ) / 90 );
        _bottomViewLayout.constant = 0 ;
        [self.view layoutIfNeeded];
        
        _bottomViewLeadingLayout.constant = 0 ;
        [self.view layoutIfNeeded];
        
        _save_X_pos.constant = 0 ;
        [self.view layoutIfNeeded];
        
        [self.imagesScrollView setScrollEnabled:YES];
        
        
        
        
    }
    else  if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft)  )
    {
        
        
        [self landscapeRotationbuttons];
        
        checkPortrait = false ;
        
        [self.imagesScrollView setScrollEnabled:NO];
        
        
        
    }
    else  if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight))
    {
        [self landscapeRotationbuttons];
        
        checkPortrait = false ;
        
        [self.imagesScrollView setScrollEnabled:NO];
        
        
    }
    
    
}

-(void)adskipTimer{
    
    currSeconds= 5;
    // [_btnAd setTitle:[NSString stringWithFormat:@"You can skip in %ds",currSeconds] forState:UIControlStateNormal];
    [_adLbl setText:[NSString stringWithFormat:@"You can skip in %ds",currSeconds]];
    
    _adLbl.backgroundColor=[UIColor clearColor];
    
    [self start];
    
}

-(void)start
{
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}
-(void)timerFired
{
    if(currSeconds>0)
    {
        currSeconds-=1;
        
        // [_btnAd setTitle:[NSString stringWithFormat:@"You can skip in %ds",currSeconds] forState:UIControlStateNormal];
        
        
        [_adLbl setText:[NSString stringWithFormat:@"You can skip in %ds",currSeconds]];
        
        
    }
    
    
    if(currSeconds == 0){
        [timer invalidate];
        
        self.adLbl.hidden = YES ;
        self.btnAd.hidden = NO ;
        
        
        [_btnAd setTitle:[NSString stringWithFormat: @"Skip Advertisement"] forState:UIControlStateNormal];
        
        // [_adLbl setText:[NSString stringWithFormat: @"Skip Advertisement"]];
        
    }
    
}
- (IBAction)skipAd:(id)sender {
    [self.btnMenu setHidden:false];
    [self.AdView setHidden:YES];
}


-(BOOL)shouldAutorotate
{
    return YES;
}


-(void)rotationMethod{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstTime"] == true)
    {
        [self addPortraitLandscapeScrollView];
        
        
        // [self.view addSubview:_imagesScrollView];
        _imagesScrollView.delegate = self;
        [self.view sendSubviewToBack:_imagesScrollView];
        
        
        [self.view bringSubviewToFront:_info_Btn];
        [self.view sendSubviewToBack:_scrllView];
        [_imagesScrollView setPagingEnabled:YES];
        [_imagesScrollView setAlwaysBounceVertical:NO];
        
        
        _imageView.multipleTouchEnabled = YES;
        [ _imageView canBecomeFirstResponder];
        _imageView.userInteractionEnabled = YES;
        
    }
    else{
        
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"orientation"] == true)
    {
        [self addPortraitLandscapeScrollView];
        _imagesScrollView.delegate = self;
        [self.view sendSubviewToBack:_imagesScrollView];
        
        
        [self.view bringSubviewToFront:_info_Btn];
        [self.view sendSubviewToBack:_scrllView];
        [_imagesScrollView setPagingEnabled:YES];
        [_imagesScrollView setAlwaysBounceVertical:NO];
        
        
        _imageView.multipleTouchEnabled = YES;
        [ _imageView canBecomeFirstResponder];
        _imageView.userInteractionEnabled = YES;
        
        
        
    }else{
        
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
    
    
}


-(void)landscapeRotationbuttons{
    
    
    
    _topTimeLayout.constant = self.view.frame.size.height/2 - 30 ;
    [self.view layoutIfNeeded];
    
    _topDaysLayout.constant = self.view.frame.size.height/2 - 10;
    [self.view layoutIfNeeded];
    
    _time_X_Pos.constant = self.view.frame.size.width/3 ;
    [self.view layoutIfNeeded];
    
    _day_X_pos.constant = self.view.frame.size.width/4  ;
    [self.view layoutIfNeeded];
    
    _save_Y_pos.constant = 0 ;
    [self.view layoutIfNeeded];
    
    _save_X_pos.constant = -30 ;
    [self.view layoutIfNeeded];
    
    
    _bottomView.transform=CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
    
    _bottomViewLayout.constant = self.view.frame.size.height/2 - 15 ;
    [self.view layoutIfNeeded];
    
    
    _bottomViewLeadingLayout.constant = -self.view.frame.size.width +50  ;
    [self.view layoutIfNeeded];
    
}




- (void)SHARE{
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToTwitter];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)slideVideo:(id)sender {
    //    [_av_player pause];
    //
    //
    //
    //    [_av_player play];
    
}




-(void)getThemePointsFromServer{
    
    
    
    NSString * apiURLStr = [NSString stringWithFormat:@"https://artlords.com/api/theme_purchases/points_spent"];
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:apiURLStr]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                
                if (data ==nil) {
                    
                }
                else{
                    id resultt = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                    NSLog(@"%@",resultt);
                    
                    totalPointsSpent = [[resultt valueForKey:@"points"] intValue];
                    
                    
                    
                }
                
            }] resume];
    
    
    
}


-(void)postThemePurchase{
    
    NSString *avg = @"1";
    
    
    NSLog(@"%@",pointsNeeded);
    
    NSString * apiURLStr = [NSString stringWithFormat:@"https://artlords.com/api/theme_purchases?udid=%@&theme_details_id=%@&avg_value=%@&points=%@",user_id,theme_detailId,avg,pointsNeeded];
    
    
    NSData* responseData = nil;
    NSURL *url = [NSURL URLWithDataRepresentation:[apiURLStr dataUsingEncoding:NSUTF8StringEncoding] relativeToURL:nil];
    responseData = [NSMutableData data] ;
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    NSString *bodydata=[NSString stringWithFormat:@"data=%@",apiURLStr];
    
    [request setHTTPMethod:@"POST"];
    NSData *req=[NSData dataWithBytes:[bodydata UTF8String] length:[bodydata length]];
    [request setHTTPBody:req];
    
    NSURLResponse* response;
    NSError* error = nil;
    responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (responseData != nil)
    {
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        id jsonResponseData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
        NSLog(@"%@",jsonResponseData);
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        NSLog(@"the final output is:%@",responseString);
        
        
        
    }
}


-(void)getAllThemesDataFromServer{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //        [KVNProgress dismiss];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message: @"No Network Connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }else{
        
        NSString *strUrl = [NSString stringWithFormat:@"https://artlords.com/api/themes/"];
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strUrl]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
        
        [request setValue:@"Token token=f7e31f33930d0351f99e09d81e708b51e906c3eb34d8af71" forHTTPHeaderField:@"Authorization"];
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        
        [request setHTTPMethod:@"GET"];
        
        
        
        
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            request.cachePolicy = NSURLRequestReloadIgnoringCacheData ;
            if (data!=nil) {
                
                self.arrayThemeData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                NSLog(@"%@", self.arrayThemeData);
                
                
                [self performSelectorOnMainThread:@selector(updateCategoryData) withObject: nil waitUntilDone:YES];
                
            }
            
            
        }];
        [dataTask resume];
        
        
    }
    
}
-(void)updateCategoryData{
    NSMutableArray*Categories = [NSMutableArray new];
    NSMutableDictionary*dict = [NSMutableDictionary new];
    
    
    for (dict  in _arrayThemeData) {
        
        [Categories addObject:dict[@"category"]];
        
        
    }
    
    NSLog(@"%@",Categories);
    
    
    
    
    int j = (int)Categories.count ;
    NSMutableDictionary*dct = [NSMutableDictionary new];
    NSArray *arrayKey = [NSArray new];
    
    for(int i = 0; i < [menuItemsArray count]; i++) {
        
              
        
        [dct setValue:menuItemsArray[i] forKey:[NSString stringWithFormat:@"%@keys",menuItemsArray[i]]];
        
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"category == %@",menuItemsArray[i]];
        
        arrayKey = [_arrayThemeData filteredArrayUsingPredicate:pred];
        
        [arrayAdd insertObject:arrayKey atIndex:i];
        
    }
    
    NSLog(@"%@",dct);
    NSLog(@"%@",arrayKey);
    NSLog(@"%@",arrayAdd);
    
    
    
    NSString *substring = @"Fantasy";
    
  
    
}


-(void)savingThemesBycategory{
    
         AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        NSLog(@"%@",keys);
        
        
        
        if ([_strKey isEqualToString:@"Creature"]) {
            
            
            
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            [SSKeychain setPassword:[delegate.imagesArray componentsJoinedByString:@","] forService:@"SavingImages" account:@"Creature"];
            
            
            [delegate.myCollectionArray addObject:[delegate.Creaturekeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Creaturekeys objectAtIndex:pageNo]];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Creaturekeys removeObjectAtIndex:pageNo];
            
            
            
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                
                delegate.isNoRecord = false;
            }else{
                
                delegate.isNoRecord = true ;
                
            
                
                
                
            }
            
            
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            
        } else if ([_strKey isEqualToString:@"Featured"]) {
            
            
            
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.featuredkeys objectAtIndex:pageNo]];
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.featuredkeys removeObjectAtIndex:pageNo];
            
            NSArray *arrKey = [delegate.featuredkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"FeaturedData"];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                 delegate.isNoRecord = false;
                
                
                
            }else{
                
                delegate.isNoRecord = true;
                
                delegate.isPurchsedFeatured = @"Yes" ;
                
                [SSKeychain setPassword:delegate.isPurchsedFeatured forService:@"arrayKey" account:@"isPurchsedFeatured"];
                
            }
            
            
        }
        else if ([_strKey isEqualToString:@"CrimsonCarrot"]) {
            
            
            
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.crimpsonCarrotKeys objectAtIndex:pageNo]];
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.crimpsonCarrotKeys removeObjectAtIndex:pageNo];
            
            NSArray *arrKey = [delegate.crimpsonCarrotKeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"CrimsonData"];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                delegate.isNoRecord = false;
                
                
                
            }else{
                
                delegate.isNoRecord = true;
                
                delegate.isPurchsedCrimson = @"Yes" ;
                
                [SSKeychain setPassword:delegate.isPurchsedCrimson forService:@"arrayKey" account:@"isPurchsedCrimson"];
                
            }
            
            
        }

        
        else if ([_strKey isEqualToString:@"Live"]) {
            
            
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.liveKeys objectAtIndex:pageNo]];
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.liveKeys removeObjectAtIndex:pageNo];
            
            NSArray *arrKey = [delegate.liveKeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"LiveData"];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
                
            }else{
                
                  delegate.isNoRecord = true;
                delegate.isPurchsedLive = @"Yes" ;
                
                [SSKeychain setPassword:delegate.isPurchsedLive forService:@"arrayKey" account:@"isPurchsedLive"];
            }
            
            
        }
        
        
        else  if ([_strKey isEqualToString:@"Character"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.characterkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.characterkeys objectAtIndex:pageNo]];
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.characterkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }else{
                
                  delegate.isNoRecord = true;
            }
            
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            
        }
        else  if ([_strKey isEqualToString:@"Concept Art"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.conceptArtkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.conceptArtkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.conceptArtkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }else{
                
                  delegate.isNoRecord = true;
            }
            
            
            
        }
        else  if ([_strKey isEqualToString:@"Game Art"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            [delegate.myCollectionArray addObject:[delegate.gameArtkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.gameArtkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            
            [delegate.gameArtkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }
            else{
                
                  delegate.isNoRecord = true;
            }
            
            
        }
        else  if ([_strKey isEqualToString:@"Comic Art"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.ComicArtkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.ComicArtkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.ComicArtkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }else{
                
                  delegate.isNoRecord = true;
            }
            
            
            
        }
        else  if ([_strKey isEqualToString:@"Vehicle"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            [delegate.myCollectionArray addObject:[delegate.vehiclekeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.vehiclekeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.vehiclekeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                
                  delegate.isNoRecord = false;
            }else{
              delegate.isNoRecord = true;
                
            }
            
        }
        else  if ([_strKey isEqualToString:@"Horror"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.Horrorkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Horrorkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Horrorkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }else{
                
                  delegate.isNoRecord = true;
            }
            
            
        }
        
        else  if ([_strKey isEqualToString:@"Environment"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.Environmentkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Environmentkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Environmentkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                  delegate.isNoRecord = false;
            }else{
                
                  delegate.isNoRecord = true;
            }
            
            
        }else  if ([_strKey isEqualToString: @"Fantasy"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
            }
            
            
            
            [delegate.myCollectionArray addObject:[delegate.Fantasykeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Fantasykeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Fantasykeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                 delegate.isNoRecord = false;
            }else{
                
                 delegate.isNoRecord = true;
            }
            
            
            
        }
        else  if ([_strKey isEqualToString: @"IndustrialDesign"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            [delegate.myCollectionArray addObject:[delegate.IndustrialDesignkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.IndustrialDesignkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.IndustrialDesignkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                 delegate.isNoRecord = false;
            }else{
             delegate.isNoRecord = true;
                
            }
            
            
        }else  if ([_strKey isEqualToString:  @"Illustration"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            
            [delegate.myCollectionArray addObject:[delegate.Illustrationkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Illustrationkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Illustrationkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                 delegate.isNoRecord = false;
            }else{
                
                 delegate.isNoRecord = true;
            }
            
            
            
        }else  if ([_strKey isEqualToString:@"ScienceFiction"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.ScienceFictionkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.ScienceFictionkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.ScienceFictionkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                 delegate.isNoRecord = false;
            }else{
                 delegate.isNoRecord = true;
            }
            
            
            
            
        }else  if ([_strKey isEqualToString:@"Landscape"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            
            [delegate.myCollectionArray addObject:[delegate.Landscapekeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Landscapekeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Landscapekeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                delegate.isNoRecord = false;
            }else{
                delegate.isNoRecord = true;
            }

            
            
            
        }else  if ([_strKey isEqualToString: @"DigitalArt"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.DigitalArtkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.DigitalArtkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.DigitalArtkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                delegate.isNoRecord = false;
            }else{
                delegate.isNoRecord = true;
            }
            
            
        }
        else  if ([_strKey isEqualToString: @"Portrait"]) {
            [delegate.imagesArray removeObjectAtIndex :pageNo];
            
            [delegate.myCollectionArray addObject:[delegate.Portraitkeys objectAtIndex:pageNo]];
            [delegate.allkeys removeObject:[delegate.Portraitkeys objectAtIndex:pageNo]];
            NSArray *arrKey = [delegate.allkeys valueForKey:@"id"];
            
            [SSKeychain setPassword:[NSString stringWithFormat:@"%@",[arrKey componentsJoinedByString:@","]]   forService:@"arrayKey" account:@"allkeysData"];
            
            [delegate.arrayKey removeObjectAtIndex:pageNo];
            [delegate.Portraitkeys removeObjectAtIndex:pageNo];
            
            idd = [[NSMutableArray alloc]initWithArray:[delegate.arrayKey valueForKey:@"id"]];
            if (idd.count>0) {
                strId = [NSString stringWithFormat:@"%@", [idd objectAtIndex:0]];
                NSLog(@"%@",strId);
                delegate.isNoRecord = false;
            }else{
                delegate.isNoRecord = true;
            }

            
        }
        
}



- (IBAction)donePickerBtn:(id)sender {
    
    
    [self ShowSelectedDate];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.bottomPickerlayout.constant = -220 ;
        [self.view layoutIfNeeded];
        
        
    }];
    
    

}


-(void)ShowSelectedDate
{
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    self.txtBirthDate.font = [UIFont systemFontOfSize:12.0f];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[formatter stringFromDate:_datePicker.date]];
    //  NSString *newString = [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    self.txtBirthDate.text = str;
    
    [self.txtBirthDate resignFirstResponder];
    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.bottomPickerlayout.constant = -220 ;
        [self.view layoutIfNeeded];
        
        
    }];
    
    
    
    [textField resignFirstResponder];
    return true;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    if  (textField == self.txtBirthDate){
       
        
        [self.txtBirthDate resignFirstResponder];
        
            [UIView animateWithDuration:0.5 animations:^{
                
               
                self.bottomPickerlayout.constant = 0 ;
                 [self.view layoutIfNeeded];
               
                
            }];
        
         [self.view bringSubviewToFront:_viewPicker];
        
        
    }

    return NO;

         
         
}

- (IBAction)enterCrimsonClicked:(id)sender {
    
    
    
    NSDate *dateA = [NSDate date];
   
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init] ;
    [df1 setDateFormat:@"MM-dd-yyyy"];
    NSDate *dateB = [df1 dateFromString:self.txtBirthDate.text];
   
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                               fromDate:dateB
                                                 toDate:dateA
                                                options:0];
    
    NSLog(@"Difference in date components: %li/%li/%li", (long)components.day, (long)components.month, (long)components.year);
    
    
    
    if (components.year < 13) {
        UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:nil message:@"You must be at least 13 years old  to view this content." delegate: nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
        self.crimpsonFirstView.hidden =  NO;
        
        
    } else {
        //Ok Age
        
        NSString *plusThirteen = @"YES";
        
    [SSKeychain setPassword: plusThirteen forService:@"plusThirteen" account:@"plusThirteen"];
        
        self.crimpsonFirstView.hidden =  YES ;
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        viewLabel.textColor  =[UIColor colorWithRed:250/255.0f green:189/255.0f blue:36/255.0f alpha:1];
        
        delegate.imagesArray = [[NSMutableArray alloc]init];
        
        delegate.arrayKey =  [[NSMutableArray alloc]initWithArray:delegate.crimpsonCarrotKeys];
        self.strKey = @"CrimsonCarrot";
        if (delegate.arrayKey.count >0) {
            
            
            delegate.isNoRecord = false ;
            
            NSLog(@"%@",delegate.imagesArray);
            for (NSMutableDictionary * data in delegate.crimpsonCarrotKeys) {
                NSString *strImage =  [NSString stringWithFormat:@"%@.jpg",data[@"name"]];
                if ([strImage containsString:@"mp4"]) {
                    strImage = [strImage stringByReplacingOccurrencesOfString:@".jpg"
                                                                   withString:@""];
                    
                }
                
                [delegate.imagesArray addObject:strImage];
            }
            
            NSLog(@"%@",delegate.imagesArray);
            
            
            [self getDescription];
            [self scrollimages];
            [self closeTableClicked:nil];
            [self getImagesFromServer];
            
            _slider.hidden = YES;
            _landscapeSlider.hidden = YES;
            
            
        }
        else{
            delegate.isNoRecord = true;
            [self noThemesMethod];
            
        }
        
        
    }
}

- (IBAction)closeBtn:(id)sender {
    
     self.crimpsonFirstView.hidden =  YES ;
    
    [self menuShow];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.bottomPickerlayout.constant = -220 ;
        [self.view layoutIfNeeded];
        
        
    }];
    
    
}
@end
