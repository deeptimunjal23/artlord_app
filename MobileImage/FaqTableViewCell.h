//
//  FaqTableViewCell.h
//  PocketSnap
//
//  Created by Deft Desk on 23/08/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaqTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *viewFaq;
@property (strong, nonatomic) IBOutlet UILabel *lblQus;
@property (strong, nonatomic) IBOutlet UILabel *lblAnsr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightQuest;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAnswer;
@property (strong, nonatomic) IBOutlet UILabel *grayBar;
@end
