//
//  HelpVC.m
//  MobileImage
//
//  Created by DeftDesk on 01/03/17.
//  Copyright © 2017 Anil. All rights reserved.
//

#import "HelpVC.h"
#import "FaqTableViewCell.h"

@interface HelpVC (){
    NSMutableArray *faqArray;
    NSInteger lastIndex;
    BOOL reloadTblVw;
    BOOL closeSection;
}
@property (strong, nonatomic) UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UITableView *tblFaq;

@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *rushCellNib = [UINib nibWithNibName:@"FaqTableViewCell" bundle:nil];
    [self.tblFaq registerNib:rushCellNib forCellReuseIdentifier:@"FaqTableViewCell"];
    faqArray =[[NSMutableArray alloc]initWithObjects:@"njewjfejw;djedjew;jd ejdejldjljdl jakdskjfdjdkjsdkj", nil];

   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark UITableView Delegate Methods
#pragma mark

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *reuseIdentifier = @"FaqTableViewCell";
    
    FaqTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    //    if(!cell)
    //    {
    //
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        cell.textLabel.numberOfLines = 0;
    //        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //
    //    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = cell.contentView.backgroundColor;
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0.001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    int heightQuest = 40;
    int heightAnswer = 70;
    
//    NSAttributedString *attrQuest = (NSAttributedString *) faqArray[section];
//    
//    CGRect rect = [attrQuest boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 35, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//    if (rect.size.height > 27) {
//        heightQuest = rect.size.height;
//    }
//    
//    
//    NSAttributedString *attrAnswer = (NSAttributedString *) faqArray[section];
//    
//    CGRect rect1 = [attrAnswer boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 48, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//    if (rect1.size.height > 32) {
//        heightAnswer = rect1.size.height;
//    }
    
    if (closeSection)
    {
        return heightQuest + 8;
    }
    else if (reloadTblVw && lastIndex == section)
    {
        return heightQuest + heightAnswer + 10;
    }
    return heightQuest + 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *cellIdentifier = @"FaqTableViewCell";
    
    FaqTableViewCell *cell = (FaqTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor =[UIColor colorWithRed:189/255.0f green:187/255.0f blue:188/255.0f alpha:1];
    cell.lblQus.userInteractionEnabled = YES;
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    cell.lblQus.tag = section;
    [cell.lblQus addGestureRecognizer:headerTapped];
    
    
//    NSAttributedString *attrQuest = (NSAttributedString *) faqArray[section];
//    
//    CGRect rect = [attrQuest boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 35, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//    
//    cell.lblQus.attributedText = attrQuest;
//    
//    if (rect.size.height > 27) {
//        cell.heightQuest.constant = rect.size.height;
//    }
//    
//    NSAttributedString *attrAnswer = (NSAttributedString *) faqArray[section];
//    
//    CGRect rect1 = [attrAnswer boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 48, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
//    
//    cell.lblAnsr.attributedText = attrAnswer;
//    
    
//    if (rect1.size.height > 32) {
//        cell.heightAnswer.constant = rect1.size.height;
//    }
    
    [cell layoutIfNeeded];
    
    return cell;
    
}
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    FaqTableViewCell *cell = [[FaqTableViewCell alloc]init];
    cell.grayBar.hidden = YES;
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    // [arrayTitle replaceObjectAtIndex:indexPath.section withObject:[UIImage imageNamed:@"arrowUpBtn"]];
    
    if (reloadTblVw) {
        if (lastIndex == gestureRecognizer.view.tag && !closeSection) {
            closeSection = true;
            //[arrayTitle replaceObjectAtIndex:indexPath.section withObject:[UIImage imageNamed:@"arrowDownBtn"]];
            
        }
        else {
            closeSection = false;
        }
    }
    else {
        closeSection = false;
    }
    reloadTblVw = true;
    lastIndex = indexPath.section;
    
    [self.tblFaq reloadData];
    
}


- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
