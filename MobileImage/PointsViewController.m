//
//  PointsViewController.m
//  MobileImage
//
//  Created by DeftDesk on 19/12/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import "PointsViewController.h"
#import "customPointView.h"
#import "AppDelegate.h"
#import "customFreePoints.h"

@interface PointsViewController ()

@end

@implementation PointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    _btnClose.layer.cornerRadius = _btnClose.bounds.size.width/2;
//    [_btnClose setClipsToBounds:YES];
//    _btnClose.layer.borderWidth = 1;
//    _btnClose.layer.borderColor = [UIColor grayColor].CGColor;

    
    [self postThemePurchase];
    
      [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
   
    _productsRequest.delegate = self;
    
    coinsLblArray = [[NSMutableArray alloc]initWithObjects:@"Pile of Points",@"2 Piles of Points",@"Bag of Points",@"Box of Points",@"2 Boxes of Points",@"Chest of Points", nil];
    
      coinsCountArray = [[NSMutableArray alloc]initWithObjects:@"100",@"225",@"550 ",@"1100 ",@"2200",@"5400", nil];
    
      coinsPriceArray = [[NSMutableArray alloc]initWithObjects:@"$.99",@"$1.99",@"$4.99",@"$9.99",@"$19.99",@"$49.99", nil];
    
    coinsPointsArray = [[NSMutableArray alloc]initWithObjects:@"+0 Bonus Points",@"+25 Bonus Points",@"+50 Bonus Points",@"+100 Bonus Points",@"+200 Bonus Points",@"+400 Bonus Points", nil];
    
    _txtPoints.text = [NSString stringWithFormat:@"%@",_strCoins];
    imagesArray = [[NSMutableArray alloc]initWithObjects:@"111.png",@"111.png",@"222.png",@"33.png",@"33.png",@"49.png", nil];
    
    pointsArray = [[NSMutableArray alloc]initWithObjects:@"100Coins",@"220Coins",@"550Coins",@"1100Coins",@"2200Coins",@"5400Coins", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate setShouldRotate:NO];
//    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
//    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
     [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return coinsPriceArray.count ;
    
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (indexPath.row == coinsPriceArray.count) {
//        
//        customFreePoints *cell = [tableView dequeueReusableCellWithIdentifier:@"customFreePoints"];
//        if (cell==nil) {
//            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"customFreePoints" owner:self options:nil];
//            
//            cell= arr[0];
//            
//        }
//        
//        return cell ;
//    }else{

        customPointView *cell = [tableView dequeueReusableCellWithIdentifier:@"customPointView"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"customPointView" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        cell.alpha = .85;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [_CoinsTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
        cell.txtCount.text = [coinsCountArray objectAtIndex:indexPath.row];
        cell.txtPrice.text = [coinsPriceArray objectAtIndex:indexPath.row];
        cell.txtlbl.text = [coinsLblArray objectAtIndex:indexPath.row];
        
    
    cell.txtpoints.text = [coinsPointsArray objectAtIndex:indexPath.row];
    cell.imgPoints.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    
    
    
        [cell.btnBuy addTarget:self action:@selector(btnBuy:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnBuy.tag = indexPath.row +1;
        
        
        return cell;
        
//    }
//    
//    return nil ;
   
    
}

-(void)btnBuy:(id)sender{
    UIButton *button = (UIButton *)sender;
    NSLog(@"%ld",button.tag);
   
    if (button.tag == 1) {
    askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 100 coins for $0.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 1;
        
        [askToPurchase show];

    }
    else if (button.tag == 2) {
        askToPurchase = [[UIAlertView alloc]
                                       initWithTitle:@"Message"
                                       message:@"Do you want to Buy 225 coins for $1.99 ?"
                                       delegate:self
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
          askToPurchase.tag = 2;
        [askToPurchase show];

        
    }
   else if (button.tag == 3) {
       askToPurchase = [[UIAlertView alloc]
                                      initWithTitle:@"Message"
                                      message:@"Do you want to Buy 550 coins for $4.99 ?"
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"Yes", @"No", nil];
       askToPurchase.delegate = self;
       askToPurchase.tag = 3;;
       [askToPurchase show];

        
    }
   else if (button.tag == 4) {
      askToPurchase = [[UIAlertView alloc]
                                      initWithTitle:@"Message"
                                      message:@"Do you want to Buy 1100 coins for $9.99 ?"
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"Yes", @"No", nil];
       askToPurchase.delegate = self;
         askToPurchase.tag = 4;
       [askToPurchase show];

        
    }
   else if (button.tag == 5) {
       askToPurchase = [[UIAlertView alloc]
                                      initWithTitle:@"Message"
                                      message:@"Do you want to Buy 2200 coins for $19.99 ?"
                                      delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"Yes", @"No", nil];
       askToPurchase.delegate = self;
         askToPurchase.tag = 5;
       [askToPurchase show];

        
    }
  else if (button.tag == 6) {
      askToPurchase = [[UIAlertView alloc]
                                     initWithTitle:@"Message"
                                     message:@"Do you want to Buy 5400 coins for $49.99?"
                                     delegate:self
                                     cancelButtonTitle:nil
                                     otherButtonTitles:@"Yes", @"No", nil];
      askToPurchase.delegate = self;
        askToPurchase.tag = 6;
      [askToPurchase show];

        
    }
   
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
   
        if (buttonIndex==0) {
            ProductIdStr = @"" ;
            
            
            if (askToPurchase.tag== 1) {
                
                NSString *str100 = @"100" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str100]intValue];
                NSLog(@"%ld",(long)coinsCount);
              
                ProductIdStr = @"100Coins" ;

                
                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                    
                    
                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[0]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments due to parental controls");
                    
                }

                
                NSLog(@"%@",payment);
                
            }else if (askToPurchase.tag==2){
                NSString *str250 = @"225" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str250]intValue];
                
                
                NSLog(@"%ld",(long)coinsCount);
               // payment = [SKPayment paymentWithProductIdentifier:[NSString stringWithFormat:@"%@",[pointsArray objectAtIndex:1]]];
                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                     ProductIdStr = @"220Coins" ;
                    
                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[1]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments due to parental controls");
                    
                }
                NSLog(@"%@",payment);

                
            }else if (askToPurchase.tag==3){
               
                NSString *str500 = @"550" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str500]intValue];
              //  payment = [SKPayment paymentWithProductIdentifier:[NSString stringWithFormat:@"%@",[pointsArray objectAtIndex:2]]];
                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                    ProductIdStr = @"550Coins" ;

                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[2]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments due to parental controls");
                    
                }
                NSLog(@"%@",payment);
                NSLog(@"%ld",(long)coinsCount);
                
            }
            else if (askToPurchase.tag==4){
                NSString *str1000 = @"1100" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str1000]intValue];
                ProductIdStr = @"1100Coins" ;

                
                
                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                    
                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[3]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments due to parental controls");
                    
                }
                NSLog(@"%@",payment);
                NSLog(@"%ld",(long)coinsCount);

                
            }
            else if (askToPurchase.tag==5){
                NSString *str2000 = @"2200" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str2000]intValue];
            //payment = [SKPayment paymentWithProductIdentifier:[NSString stringWithFormat:@"%@",[pointsArray objectAtIndex:4]]];
                ProductIdStr = @"2200Coins" ;

                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                    
                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[4]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments dto parental controls");
                    
                }
                NSLog(@"%@",payment);
                NSLog(@"%ld",(long)coinsCount);

                
    
                
            } else if (askToPurchase.tag==6){
                NSString *str5000 = @"5400" ;
                coinsCount = [[NSString stringWithFormat:@"%@",str5000]intValue];
                ProductIdStr = @"5400Coins" ;

               // payment = [SKPayment paymentWithProductIdentifier:[NSString stringWithFormat:@"%@",[pointsArray objectAtIndex:5]]];
                if([SKPaymentQueue canMakePayments]){
                    NSLog(@"User can make payments");
                    
                    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:pointsArray[5]]];
                    productsRequest.delegate = self;
                    [productsRequest start];
                    
                }
                else{
                    NSLog(@"User cannot make payments due to parental controls");
                    
                }
                NSLog(@"%@",payment);
                NSLog(@"%ld",(long)coinsCount);

                
            }
            
            
          //  [[SKPaymentQueue defaultQueue] addPayment:payment];
            
           

            
           // [self coinsPurchased];
           
            
        }
    }
    


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 100 coins for $0.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 1;
        
        [askToPurchase show];
        
    }
    else if (indexPath.row == 1) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 225 coins for $1.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 2;
        [askToPurchase show];
        
        
    }
    else if (indexPath.row == 2) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 550 coins for $4.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 3;;
        [askToPurchase show];
        
        
    }
    else if (indexPath.row == 3) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 1100 coins for $9.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 4;
        [askToPurchase show];
        
        
    }
    else if (indexPath.row == 4) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 2200 coins for $19.99 ?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 5;
        [askToPurchase show];
        
        
    }
    else if (indexPath.row == 5) {
        askToPurchase = [[UIAlertView alloc]
                         initWithTitle:@"Message"
                         message:@"Do you want to Buy 5400 coins for $49.99?"
                         delegate:self
                         cancelButtonTitle:nil
                         otherButtonTitles:@"Yes", @"No", nil];
        askToPurchase.delegate = self;
        askToPurchase.tag = 6;
        [askToPurchase show];
        
        
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115 ;
    
}


- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
   
    int count = (int)[response.products count];
    if (count>0) {
       
        SKPayment *paymentt = [SKPayment paymentWithProductIdentifier:ProductIdStr];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:paymentt];
        
        
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:nil
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
        
    }

    
}



-(void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    NSLog(@"in removedTransactions method of SKPaymentTransactionObserver");
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"in restoreCompletedTransactionFailedWithError method of SKPaymentTransactionObserver");
    NSLog(@"error in restoreCompletedTransactionFailedWithError is %@", error.description);
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"in paymentQueueRestoreCompletedTransactionsFinished method of SKPaymentTransactionObserver");
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"in requestDidFailWith method of SKRequestDelegate error is %@", error.description);
}

-(void)requestDidFinish:(SKRequest *)request
{
    NSLog(@"in requestDidFinish method of SKRequestDelegate");
}

-(void)coinsPurchased{
    
    NSString *str  = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"coins"]];
    
    int remainingCoins = [str intValue];
    
    
    coinsCount = coinsCount + remainingCoins ;
    
    _txtPoints.text = [NSString stringWithFormat:@"%d",coinsCount];
    
    [[NSUserDefaults standardUserDefaults]setInteger:coinsCount forKey:@"coins"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}




-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"in paymentQueueMethod of SKPaymentTransactionObserver");
    NSLog(@"initial count of transactions %lu", (unsigned long)transactions.count);
    
    
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier isEqualToString:@"100Coins"])
                {
                    
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                    [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];

                   
                    
                }
                else if([transaction.payment.productIdentifier isEqualToString:@"220Coins"]) {
                    
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                    [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];
                    
                    
                }
                else if([transaction.payment.productIdentifier isEqualToString:@"550Coins"]) {
                    
                    
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                     [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];
                    
                   

                }
                else if([transaction.payment.productIdentifier isEqualToString:@"1100Coins"]) {
                    
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                    [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];
                    
                }
                else if([transaction.payment.productIdentifier isEqualToString:@"2200Coins"]) {
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                     [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];
                    
                }
                else if([transaction.payment.productIdentifier isEqualToString:@"5400Coins"]) {
                    [self coinsPurchased];
                    NSLog(@"Purchased ");
                     [SSKeychain setPassword:_txtPoints.text forService:@"image" account:@"imagelordData"];
                    
                }
            
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                 
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)postReqPurchase{
    
    NSString *userId = @"1";
    NSString *theme_detail = @"1";
    NSString *avg = @"1";
    NSString *points = @"100" ;
    
    
    NSString * apiURLStr = [NSString stringWithFormat:@"https://artlords.com/api/theme_purchases"];
    NSString *dataStr = [NSString stringWithFormat:@"\"udid\":\"%@\",\"theme_details_id\":\"%@\",\"avg_value\":\"%@\",\"points\":\"%@\"",userId,theme_detail,avg,points];
    
    
    NSLog(@"%@",dataStr);
    
    
    NSString *strpostlength=[NSString stringWithFormat:@"%lu",(unsigned long)[dataStr length]];
    NSMutableURLRequest *urlrequest=[[NSMutableURLRequest alloc]init];
    
    [urlrequest setURL:[NSURL URLWithString:apiURLStr]];
    [urlrequest setHTTPMethod:@"POST"];
    [urlrequest setValue:strpostlength forHTTPHeaderField:@"Content-Length"];
    [urlrequest setHTTPBody:[dataStr dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlrequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                    id  jsonResponseData=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                                      NSLog(@"%@",jsonResponseData);
                                      NSNumber *status = jsonResponseData[@"status"];
                                     
                                      
                                  }];
    

    
    [task resume];
}

-(void)postThemePurchase{


NSString *theme_detail = @"1";
NSString *avg = @"1";
NSString *points = @"100" ;


NSString * apiURLStr = [NSString stringWithFormat:@"https://artlords.com/api/theme_purchases?udid=%@&theme_details_id=%@&avg_value=%@&points=%@",user_id,theme_detail,avg,points];


NSData* responseData = nil;
NSURL *url = [NSURL URLWithDataRepresentation:[apiURLStr dataUsingEncoding:NSUTF8StringEncoding] relativeToURL:nil];
responseData = [NSMutableData data] ;
NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
NSString *bodydata=[NSString stringWithFormat:@"data=%@",apiURLStr];

[request setHTTPMethod:@"POST"];
NSData *req=[NSData dataWithBytes:[bodydata UTF8String] length:[bodydata length]];
[request setHTTPBody:req];

NSURLResponse* response;
NSError* error = nil;
responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
if (responseData != nil)
{
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    id jsonResponseData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
    NSLog(@"%@",jsonResponseData);
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"the final output is:%@",responseString);


    
}
}

@end
