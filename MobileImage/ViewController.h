



#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SSKeychain.h"
#import "KeychainItemWrapper.h"
#import <AVFoundation/AVFoundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface ViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>{
    BOOL showBlinkTimer;
    NSMutableArray* freeimg;
    NSMutableArray *menuItemsArray;
     int x;
    NSInteger fooIndex;
  
    int pageNo ;
   // NSMutableArray *delegate.delegate.imagesArray;
    
   
    NSArray *imgArray ;
    UIImageView *imageView;
    UIBezierPath *shadowPath;
    
    NSMutableArray*arr1;
    
    NSMutableArray *description;
    NSMutableArray *name;
    NSMutableArray *collection;
    NSMutableArray *idd;
    NSString *strId;
   
    id dataJSON;
    
 //   NSMutableArray *delegate.allkeys ;
    NSMutableArray *allkeysFeatured ;

    
    NSString *strCoinsUsed;
    NSString *themeId;
    
    
    int numberOfCoins;
    int totalcoins;
    
//    NSMutableArray * delegate.myCollectionArray ;
    NSMutableArray *imagesSortingArray;
    
    bool deepLink;
    

    NSMutableArray *keys ;
    bool infoClicked;
    
    id imagesResponseData;
    
    bool saveClicked;

    id trackviewData;
    KeychainItemWrapper *keychainItem ;
    
    int SavedCollectionCount ;
    
    NSString *videoStr ;
    UIView *subView ;
    CABasicAnimation *animation;
    
    
    
    NSTimer *timer;
    int currSeconds;
    
    BOOL checkPortrait;
    AVPlayerItem *currentItem ;
    
    int rightDraggingCount ;
    CGPoint _lastContentOffset;
    CGFloat xOrigin ;

    NSString *user_id ;
  
    
    NSString *theme_detailId ;
    NSString *pointsNeeded ;
    
    NSMutableArray *collectionName ;
   
    
    id categoryData;
    
    NSURL *fileURL ;
    
    id allThemesData ;
    
    NSMutableArray *avPlayerArray;
    int count ;
    
    AVPlayer* avPlayer;
    NSMutableArray *sliderArray;
    
    NSMutableArray *arrayAdd;
    
    
    int totalPointsSpent;
    UILabel *viewLabel;
      
BOOL isDeeplink ;
    
    BOOL isPortrait;
    
    
    UIButton *btn ;
      UIImageView *img;
    
    BOOL closeSection ;
    NSInteger  lastIndex ;
    NSMutableArray *collectionsArray;
    
    BOOL reloadTblVw;
      NSArray *arr ;
    
    int selectedIndex ;
    
}


//api url

@property (nonatomic,strong) UILongPressGestureRecognizer *lpgr;



@property (nonatomic, weak) IBOutlet GADBannerView  *bannerView;



@property(strong,nonatomic)AVAudioPlayer *audioPlayer;
@property(strong,nonatomic)AVPlayerItem *currentItem;
@property(strong,nonatomic)AVPlayer *avplayer;


@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIButton *dottedBtn;
@property (strong, nonatomic) IBOutlet UIView *subsubView;

@property (strong, nonatomic) NSMutableArray *arrayThemeData;
@property (strong, nonatomic) IBOutlet UIView *dottedView;

//@property (strong, nonatomic) NSMutableArray *arrayKey;
@property (strong, nonatomic) NSString *strKey;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *txtAdded;
- (IBAction)closeViewClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtImageName;
@property (strong, nonatomic) IBOutlet UIButton *themeSClicked;
- (IBAction)themeClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *ViewSaved;
@property (strong, nonatomic) IBOutlet UIImageView *imgSaved;
@property (strong, nonatomic) IBOutlet UILabel *txtPointsUsed;
@property (strong, nonatomic) IBOutlet UILabel *txtPointsRemain;



@property (strong, nonatomic) IBOutlet UIImage *saveImg;



@property (strong, nonatomic) IBOutlet UIScrollView *imagesScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomSaveBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondSaveTop;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveSecond;
- (IBAction)btnSaveSecond:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewMovableHeight;
@property (strong, nonatomic) IBOutlet UIView *viewMovable;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *saveBtnBottom;
@property (strong, nonatomic) IBOutlet UIScrollView *scrllView;
@property (strong, nonatomic) IBOutlet UIButton *btnSettings;
@property (strong, nonatomic) IBOutlet UIButton *btnTheme;
@property (strong, nonatomic) IBOutlet UITableView *menuTableview;
@property (strong, nonatomic) IBOutlet UIView *slidingView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)info_btn:(id)sender;
- (IBAction)setting_btn:(id)sender;
- (IBAction)imgSave_btn:(id)sender;
- (IBAction)folder_btn:(id)sender;
- (IBAction)categorie_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbe_Time;
@property (strong, nonatomic) IBOutlet UILabel *lbe_Weekdays;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) NSTimer *timeleft;
@property (strong, nonatomic) NSTimer *timeright;
@property (weak, nonatomic) IBOutlet UIView *info_view;
@property (weak, nonatomic) IBOutlet UIImageView *imgSwipe;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewinfoHight;
@property (weak, nonatomic) IBOutlet UIButton *info_Btn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *setting_btnBottom;
@property (weak, nonatomic) IBOutlet UIView *viewTopLeft;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewLeading;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menuBtnLeading;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIView *swipableView;
@property (strong, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)homeClicked:(id)sender;

- (IBAction)menuClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveBtnBottomLayOut;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *folderBtnLayOut;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewLikeLayOut;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
@property (strong, nonatomic) IBOutlet UIImageView *imgGradient;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UITextView *txtView;


- (IBAction)closeClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIButton *btnCloseTable;
- (IBAction)closeTableClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgCoin;
@property (strong, nonatomic) IBOutlet UILabel *txtCoin;

- (IBAction)btnAddpoints:(id)sender;
- (IBAction)btnCoinsClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtThemeName;
@property (strong, nonatomic) IBOutlet UILabel *txtArtist;
@property (strong, nonatomic) IBOutlet UIButton *btnCoins;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewHeightLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ViewHeightLayout;
@property (strong, nonatomic) IBOutlet UILabel *txtCoinsSave;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightCoinsLayout;
@property (strong, nonatomic) IBOutlet UILabel *txtLbl;
- (IBAction)getCoinsClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewCoinsss;
@property (strong, nonatomic) IBOutlet UILabel *themeNamee;
@property (strong, nonatomic) IBOutlet UILabel *TxtThemeAdded;
@property (strong, nonatomic) IBOutlet UILabel *txtUsedCoins;
@property (strong, nonatomic) IBOutlet UILabel *txtBalance;
- (IBAction)morePointsClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *doneClickedd;
- (IBAction)doneClickeddd:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *txtNewBalance;
@property (strong, nonatomic) IBOutlet UILabel *txtPoints;
@property (strong, nonatomic) IBOutlet UILabel *txtCoinsCount;
@property (strong, nonatomic) IBOutlet UIButton *settingsMenu;
@property (strong, nonatomic) IBOutlet UIButton *questionMenu;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSettings;
@property (strong, nonatomic) IBOutlet UIView *viewInfoThemeSave;
@property (strong, nonatomic) IBOutlet UILabel *infoThemeName;
@property (strong, nonatomic) IBOutlet UILabel *infoCollectionName;
@property (strong, nonatomic) IBOutlet UILabel *infoPointsUsed;
@property (strong, nonatomic) IBOutlet UITextView *infoDescription;
@property (strong, nonatomic) IBOutlet UIButton *infoSaveTheme;
- (IBAction)infoSaveThemeClicked:(id)sender;
- (IBAction)infoCloseClicked:(id)sender;
- (IBAction)ViewinfoCloseClick:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtSwipe;
@property (strong, nonatomic) IBOutlet UILabel *txtTap;
@property (strong, nonatomic) IBOutlet UIView *themesVieww;
@property (strong, nonatomic) IBOutlet UIButton *themeCloseBtnn;
@property (strong, nonatomic) IBOutlet UIButton *infoCloseThemeee;

@property (strong, nonatomic) IBOutlet UIButton *btnGetMore;
@property (strong, nonatomic) IBOutlet UIView *viewNoThemes;
@property (strong, nonatomic) IBOutlet UIView *viewGetStarted;
- (IBAction)getStartedClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewFirstTime;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;

@property (strong, nonatomic) IBOutlet UIButton *btnGetStarted;
@property (strong, nonatomic) IBOutlet UIView *viewSix;
@property (strong, nonatomic) IBOutlet UIView *viewThird;

@property (strong, nonatomic) IBOutlet UIView *viewFourth;
@property (strong, nonatomic) IBOutlet UIButton *btnNextFourth;

@property (strong, nonatomic) IBOutlet UIView *viewSeven;
@property (strong, nonatomic) IBOutlet UIView *viewSecond;
@property (strong, nonatomic) IBOutlet UIView *viewFive;
@property (strong, nonatomic) IBOutlet UIView *viewFirst;
@property (strong, nonatomic) IBOutlet UIButton *btnNextFirst;
- (IBAction)NextFirstClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnNextFifth;
@property (strong, nonatomic) IBOutlet UIButton *btnNextSixth;
@property (strong, nonatomic) IBOutlet UIButton *btnNextSeventh;

- (IBAction)nextSixthClicked:(id)sender;
- (IBAction)btnNextFifth:(id)sender;
- (IBAction)NextFourthClicked:(id)sender;
- (IBAction)NextSeventhClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageThird;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *themeViewTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *infoViewTop;
@property (strong, nonatomic) IBOutlet UIView *z;
@property (strong, nonatomic) IBOutlet UIButton *btnCloseee;
@property (strong, nonatomic) IBOutlet UIButton *btnCoinsCount;
@property (strong, nonatomic) IBOutlet UIButton *btnSavedThemes;
- (IBAction)SavedThemesClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnLock;
@property (strong, nonatomic) IBOutlet UIView *addPointsView;
@property (strong, nonatomic) IBOutlet UIButton *btnLandscapeCoins;
@property (strong, nonatomic) IBOutlet UIImageView *imgLock;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topLayOUT;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthCoinLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgwidthLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *yLayout;
@property (strong, nonatomic) IBOutlet UIView *viewRed;

@property (strong, nonatomic) AVPlayer * av_player;
@property (strong, nonatomic) AVPlayerLayer * av_layer;
@property (strong, nonatomic) IBOutlet UIImageView *firstLeftArrow;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewFourthTop;
@property (strong, nonatomic) IBOutlet UILabel *leftLblFirst;
@property (strong, nonatomic) IBOutlet UILabel *rightLabelFirst;
@property (strong, nonatomic) IBOutlet UILabel *lblFifth;
@property (strong, nonatomic) IBOutlet UILabel *lblSeventh;
@property (strong, nonatomic) IBOutlet UILabel *lblSixth;
@property (strong, nonatomic) IBOutlet UILabel *lblFourth;
@property (strong, nonatomic) IBOutlet UIView *AdView;
@property (strong, nonatomic) IBOutlet UILabel *adLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnAd;
- (IBAction)skipAd:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topTimeLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topDaysLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *time_X_Pos;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *day_X_pos;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *save_Y_pos;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewLeadingLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *save_X_pos;
@property (strong, nonatomic) IBOutlet UILabel *currentVideoTime;
@property (strong, nonatomic) IBOutlet UISlider *slider;
- (IBAction)slideVideo:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sliderHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sliderTop;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerVieww;

@property (strong, nonatomic) IBOutlet UIView *bottomViewSecond;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *playLayoutTrailing;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sliderLeading;

@property (strong, nonatomic) IBOutlet UISlider *landscapeSlider;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *landscapeSliderX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *landscapeSliderY;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewX;
@property (strong, nonatomic) IBOutlet UIImageView *downArrowImg;
@property (strong, nonatomic) IBOutlet UIView *crimpsonFirstView;
@property (strong, nonatomic) IBOutlet UITextField *txtBirthDate;
@property (strong, nonatomic) IBOutlet UIButton *enterCrimpson;
@property (strong, nonatomic) IBOutlet UIView *viewBirthDate;
- (IBAction)donePickerBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *donePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomPickerlayout;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)enterCrimsonClicked:(id)sender;
- (IBAction)closeBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewPicker;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *YViewFive;

@property (strong, nonatomic) IBOutlet UIView *menuBottomView;
@property (weak, nonatomic) IBOutlet UIView *vwCoins;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnCoinsWithConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coinsViewWidth;

@end

