//
//  MyThemesController.m
//  MobileImage
//
//  Created by Anil on 18/10/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import "MyThemesController.h"
#import "FreeThemeViewCell.h"
#import "SeveThemeViewCell.h"
#import "InfoController.h"
#import "SSKeychain.h"
@interface MyThemesController (){
    NSMutableArray* freeimg;
    NSMutableArray* saveimg;
    BOOL selectedButton;
}

@end

@implementation MyThemesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    NSString*str = [SSKeychain passwordForService:@"freeImg" account:@"freeImg"];
//    freeimg = [[NSMutableArray alloc]initWithArray:[str componentsSeparatedByString:@","]];
//    
//    
//    keys  = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]valueForKey:@"Keys"]];
//    
//    
//    NSString*strr = [SSKeychain passwordForService:@"Keys" account:@"Keys"];
//    if (strr != nil && strr != (id)[NSNull null]) {
//        [keys addObject:[strr componentsSeparatedByString:@","]];
//    }
//    
//    
//    arrayDescription = [[NSMutableArray alloc]initWithArray:[keys valueForKey:@"description"]];
//
//    arrayid = [[NSMutableArray alloc]initWithArray:[keys valueForKey:@"id"]];
//    arrayName = [[NSMutableArray alloc]initWithArray:[keys valueForKey:@"name"]];
    
    
    
     arrayCollection = [[NSMutableArray alloc]initWithArray:[self.arrayImages valueForKey:@"collection"]];
    self.arrayPlayer =[[NSMutableArray alloc]init];
    for (NSDictionary *dict in self.arrayImages) {
        [self.arrayPlayer addObject:@""];
    }
    
    
    
    // Do any additional setup after loading the view.
     [self.FreeTheme_CollectionView registerNib:[UINib nibWithNibName:@"FreeThemeViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    selectedButton=true;

    [self.FreeTheme_CollectionView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];

}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:      (NSInteger)section
{
    
     return self.arrayImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(collectionView==_FreeTheme_CollectionView)
    {
    
        FreeThemeViewCell  *cell = (FreeThemeViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        NSString * checkVideo = [NSString stringWithFormat:@"%@",self.arrayImages[indexPath.row][@"name"] ];
            for (id object in cell.contentView.subviews) {
                if (![object isKindOfClass:[UIImageView class]]) {
                    UIView *vw = object;
                    [vw removeFromSuperview];
                }
            }
        
            if ([checkVideo containsString:@"mp4"]) {
                
//                videoStr  = [freeimg objectAtIndex:indexPath.row];
//               
//                NSLog(@"Video Str %@",videoStr);
                
                if ([self.arrayPlayer[indexPath.item] isKindOfClass:[NSString class]]) {
                    NSArray* foo = [checkVideo componentsSeparatedByString: @"."];
                    NSString* namee = [foo objectAtIndex: 0];
                    
                    NSString*type = [foo objectAtIndex:1];
                    NSString *filepath =[[NSBundle mainBundle] pathForResource: namee ofType:type inDirectory:nil];
                    NSURL *fileURL    =   [NSURL fileURLWithPath:filepath];
                    AVPlayer *player = [AVPlayer playerWithURL:fileURL];
                    
                    AVPlayerLayer *layer = [AVPlayerLayer layer];
                    
                    [layer setPlayer:player];
                    
                    layer.frame = CGRectMake(0, 0,cell.frame.size.width, cell.frame.size.height);
                    [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
                    
                    UIView *subViews = [[UIView alloc]init];
                    
                    subViews.frame =  CGRectMake(0, 0,cell.frame.size.width,cell.frame.size.height );
                    [subViews.layer addSublayer:layer];
                    
                    [cell.contentView addSubview:subViews];
                    [self.arrayPlayer replaceObjectAtIndex:indexPath.item withObject:subViews];
                    //                [_av_player play];
 
                }else{
                    UIView *subViews = self.arrayPlayer[indexPath.item];
                    [cell.contentView addSubview:subViews];
                }
                

               

        
    }else{
        NSLog(@"%@",[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",[self.arrayImages objectAtIndex:indexPath.row]]]);
        cell.img_viewf.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",checkVideo]];
    }
        
        return cell;


}
    return nil;
    
    
}

    


- (UIImage *)thumbnailImageFromURL:(NSURL *)videoURL {
    
    NSLog(@"Video URL %@",videoURL);
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL: videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    
    return thumbnailImage;
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    
            InfoController *infoview = [[ InfoController alloc]init];
        infoview = [self.storyboard instantiateViewControllerWithIdentifier:@"info"];
    
    
    NSString *str = [[_arrayImages objectAtIndex:indexPath.row]valueForKey:@"name"];
    
                     
                      
    if ([str containsString:@"mp4"]) {
        infoview.VideoNameStr = str ;
        
    }else{
    
    infoview.imageName = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",str]];
        
    }
    
    infoview.descriptionStr = [[_arrayImages objectAtIndex:indexPath.row]valueForKey:@"description"];
    
    
    //infoview.collectionStr = [NSString stringWithFormat:@"%@",[arrayCollection objectAtIndex:indexPath.row]];

    
    infoview.collectionStr = [[_arrayImages objectAtIndex:indexPath.row]valueForKey:@"collection"];
        infoview.nameStr = [[_arrayImages objectAtIndex:indexPath.row]valueForKey:@"name"];
    

    
        [self presentViewController:infoview animated:YES completion:nil];
  
    
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    float width = collectionView.frame.size.width / 2 - 4;
     float height = collectionView.frame.size.height / 2 - 4;
    return CGSizeMake(width, height);
    
    
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{    return 2.0;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
   
    return UIEdgeInsetsMake(0,2, 0, 2);
}



- (IBAction)Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)Save_btn:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{

    self.lable_leading.constant =17.5f;
    [self.view layoutIfNeeded];
     }];
    selectedButton=YES;
    [self.FreeTheme_CollectionView reloadData];
    [self.FreeTheme_CollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:true];

}

- (IBAction)Free_btn:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
    self.lable_leading.constant =102.0f;
    [self.view layoutIfNeeded];
         }];
    selectedButton=NO;
    [self.FreeTheme_CollectionView reloadData];
    [self.FreeTheme_CollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:true];
}
- (IBAction)homeBtn:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
    {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        
    }
@end
