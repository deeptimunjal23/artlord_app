//
//  PointsViewController.h
//  MobileImage
//
//  Created by DeftDesk on 19/12/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "SSKeychain.h"

@interface PointsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver,UIAlertViewDelegate> {
    
    NSMutableArray *coinsCountArray;
    NSMutableArray *coinsPriceArray;
    NSMutableArray *coinsLblArray;
    NSMutableArray *coinsPointsArray;
    NSMutableArray*imagesArray;

    UIAlertView*  askToPurchase;
    int coinsCount;
    SKPayment *payment;
    //SKProductsRequest *payment;
    NSMutableArray *pointsArray;
    
    NSString *ProductIdStr;
    
    NSString *user_id ;
    NSString *theme_detailId ;
    
}
@property (strong, nonatomic) SKProductsRequest *productsRequest;

@property (strong, nonatomic) IBOutlet UITableView *CoinsTableview;
@property (nonatomic , retain) NSString *strCoins;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtPoints;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@end
