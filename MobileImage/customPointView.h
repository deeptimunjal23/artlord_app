//
//  customPointView.h
//  MobileImage
//
//  Created by DeftDesk on 19/12/16.
//  Copyright © 2016 Anil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customPointView : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *coinImg;
@property (strong, nonatomic) IBOutlet UILabel *txtCount;
@property (strong, nonatomic) IBOutlet UILabel *txtlbl;
@property (strong, nonatomic) IBOutlet UILabel *txtPrice;
@property (strong, nonatomic) IBOutlet UIButton *btnBuy;
@property (strong, nonatomic) IBOutlet UILabel *txtpoints;
@property (strong, nonatomic) IBOutlet UIImageView *imgPoints;

@end
